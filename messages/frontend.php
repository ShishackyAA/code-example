<?php

return [
    'sourcePath' => '@frontend',
    'languages' => ['ru-RU'],
    'translator' => 'Yii::t',
    'sort' => true,
    'removeUnused' => true,
    'markUnused' => false,
    'only' => ['*.php'],
    'except' => [
        '.svn',
        '.git',
        '.gitignore',
        '.gitkeep',
        '.hgignore',
        '.hgkeep',
        '/messages',
    ],
    'format' => 'php',
    'messagePath' => __DIR__,
    'catalog' => 'messages',
    'overwrite' => true,
];
