<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
$modules = require(__DIR__ . '/modules.php');
$translations = require(__DIR__ . '/translations.php');
$rules = require(__DIR__ . '/rules.php');

return [
    'id' => 'app-frontend',
    'language' => 'ru-RU',
    'name' => 'VENGO VISION',
    'version' => 'v 2.0',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptchaConfig',
            'siteKeyV2' => '6LdGz4IUAAAAAHCkSQPWQRXZKnlLky4HuYeQ1h77',
            'secretV2' => '6LdGz4IUAAAAACX428neHCWUgBiocw8r2UJCQA3C',
            'siteKeyV3' => '6Lda7KEUAAAAAEjFZjHxsFc1_NHNXrnFW2-2b80d',
            'secretV3' => '6Lda7KEUAAAAAOjzm4A-wgzAY_v8TDMyk5F6x-Sh'
        ],
//        'session' => [
//            'class' => 'yii\redis\Session',
//            'redis' => [
//                'hostname' => 'localhost',
//                'port' => 6379,
//                'database' => 1,
//            ]
//        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => $rules
        ],
        'request' => [
            // a secret key used to validate cookies. You may modify this key with your own one.
            'cookieValidationKey' => 'this_very_secret_string',
            'class' => 'common\components\Request',
            'web' => '/frontend/web'
        ],
        'user' => [
            'identityClass' => 'common\models\User_pg',
            'enableAutoLogin' => true,
            'loginUrl' => [
                '/authorization'
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'exception/error-page',
        ],
        'i18n' => [
            'translations' => $translations,
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'bundles' => [
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'depends' => [
                        'yii\web\JqueryAsset',
                        'yii\jui\JuiAsset',
                        'yii\bootstrap\BootstrapAsset',
                    ],
                ],
            ],
        ],
    ],
    'params' => $params,
    'defaultRoute' => '/profile/profile',
    'modules' => $modules
];
