<?php

return [
    'app*' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@frontend/messages',
        'fileMap' => [
            'app' => 'app.php',
        ],
    ],
    'modules*' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@frontend/messages',
        'fileMap' => [
            'app' => 'modules.php'
        ],
    ],
    'cnt*' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@frontend/messages',
        'fileMap' => [
            'cnt' => 'cnt.php'
        ],
    ],
    'lectures' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@frontend/messages',
        'fileMap' => [
            'lectures' => 'lectures.php',
        ],
    ],
    'rating' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@frontend/messages',
        'fileMap' => [
            'rating' => 'rating.php',
        ],
    ],
    'storage' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@frontend/messages',
        'fileMap' => [
            'lectures' => 'storage.php',
        ],
    ],
    'profile' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@frontend/messages',
    ],
    'testing' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@frontend/messages',
    ],
    'frontend*' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@frontend/messages',
    ],
    'calendar' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'fileMap' => [
            'calendar' => 'calendar.php',
        ],
    ],
    'agreement' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@frontend/modules/agreement/messages',
        'fileMap' => [
            'agreement' => 'agreement.php',
        ],
    ],
    'archive' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@frontend/modules/archive/messages',
        'fileMap' => [
            'agreement' => 'archive.php',
        ],
    ],
    'library' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@frontend/modules/corpLibrary/messages',
    ],
    'settings' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@frontend/modules/settings/messages',
        'fileMap' => [
            'settings' => 'settings.php',
        ],
    ],
    'authorization' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@frontend/modules/authorization/messages',
        'fileMap' => [
            'authorization' => 'authorization.php',
        ],
    ],
    'colleagues' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@frontend/modules/colleagues/messages',
        'fileMap' => [
            'colleagues' => 'colleagues.php',
        ],
    ],
    'events' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@frontend/messages',
        'fileMap' => [
            'events' => 'events.php',
        ],
    ],
    'pfp' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@frontend/modules/planForPosition/messages',
        'fileMap' => [
            'pfp' => 'pfp.php',
        ],
    ],
    'complaints' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@frontend/modules/complaints/messages',
        'fileMap' => [
            'complaints' => 'complaints.php',
        ],
    ],
    'business-trip' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@frontend/modules/businessTrip/messages',
        'fileMap' => [
            'complaints' => 'business-trip.php',
        ],
    ],
    'rules' => [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@frontend/modules/agreementRules/messages',
        'fileMap' => [
            'rules' => 'rules.php',
        ],
    ],
];
