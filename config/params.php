<?php
return [
    'articleUploadFolder' => 'uploads/articles',
    'defaultImage' => '/img/default.png',
    'defaultPhotoEmployee' => '/img/defaultUserPhoto.png',
];
