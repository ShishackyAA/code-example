<?php
return [
    'rating' => [
        'class' => 'app\modules\rating\Module'
    ],
    'admin-lectures' => [
        'class' => 'app\modules\lectures\Module'
    ],
    'cnt' => [
        'class' => 'app\modules\contact\Module'
    ],
    'email' => [
        'class' => 'frontend\modules\email\Module'
    ],
    'profile' => [
        'class' => 'frontend\modules\profile\Module'
    ],
    'storage' => [
        'class' => 'app\modules\storage\Module'
    ],
    'template' => [
        'class' => 'common\modules\testing\Module'
    ],
    'agreement' => [
        'class' => 'frontend\modules\agreement\Module'
    ],
    'settings' => [
        'class' => 'frontend\modules\settings\Module'
    ],
    'events' => [
        'class' => 'frontend\modules\events\Module'
    ],
    'authorization' => [
        'class' => 'frontend\modules\authorization\Module'
    ],
    'colleagues' => [
        'class' => 'frontend\modules\colleagues\Module'
    ],
    /*'output' => [
        'class' => 'frontend\modules\output\Module'
    ],
    'vacation' => [
        'class' => 'frontend\modules\vacation\Module'
    ],
    'grade' => [
        'class' => 'frontend\modules\grade\Module'
    ],*/
    'library' => [
        'class' => 'frontend\modules\corpLibrary\Module'
    ],
    'media' => [
        'class' => 'frontend\modules\media\Module'
    ],
    'archive' => [
        'class' => 'frontend\modules\archive\Module'
    ],
    'cropper' => [
        'class' => 'frontend\modules\cropper\Module'
    ],
    'plan-for-position' => [
        'class' => 'frontend\modules\planForPosition\Module',
    ],
    'support' => [
        'class' => 'frontend\modules\support\Module',
    ],
    'complaints' => [
        'class' => 'frontend\modules\complaints\Module',
    ],
    'business-trip' => [
        'class' => 'frontend\modules\businessTrip\Module',
    ],
    'agreement-rules' => [
        'class' => 'frontend\modules\agreementRules\Module',
    ],
];
