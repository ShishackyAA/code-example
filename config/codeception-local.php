<?php

return yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/main.php',
    require __DIR__ . '/main-local.php',
    [
        'components' => [
            'request' => [
                // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
                'cookieValidationKey' => 'oA6H5qcNrzagFdU9KAcLqi-rrWsvAQFG',
            ],
            'db' => [
                'class' => 'yii\db\Connection',
                'dsn' => 'pgsql:host=postgres;port=5432;dbname=vengo_test',
                'username' => 'postgres',
                'password' => '123',
                'charset' => 'utf8',
                'enableSchemaCache' => true,
                'schemaCacheDuration' => 2000000
            ],
        ],
    ]
);
