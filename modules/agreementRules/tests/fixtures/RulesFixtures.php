<?php
namespace frontend\modules\agreementRules\tests\fixtures;

use yii\test\ActiveFixture;

class RulesFixtures extends ActiveFixture
{
    public $modelClass = 'frontend\modules\agreementRules\models\RulesModel';
}