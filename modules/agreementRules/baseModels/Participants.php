<?php

namespace frontend\modules\agreementRules\baseModels;

use common\models\BaseModel;
use common\models\redis\Employee;
use frontend\modules\settings\htmlModels\EmployeeHtml;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "agreement_rules.participants".
 *
 * @property int $id
 * @property int|null $rule_id
 * @property string|null $employee_code
 * @property string|null $employee_name
 * @property int|null $version
 * @property int|null $created
 * @property string|null $img
 */
class Participants extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agreement_rules.participants';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rule_id', 'version', 'created'], 'default', 'value' => null],
            [['rule_id', 'version', 'created'], 'integer'],
            [['employee_code'], 'string', 'max' => 255],
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'employee_name',
            'img'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rule_id' => 'Rule ID',
            'employee_code' => 'Employee Code',
            'version' => 'Version',
            'created' => 'Created',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
                'value' => time(),
            ],
        ];
    }

    public static function completeParticipantsList(array $participants)
    {
        foreach ($participants as $employeeCode => $participant) {
            $participants[$employeeCode]['img'] = EmployeeHtml::getImageUrlByPath($participant->img);
        }

        return $participants;
    }
}
