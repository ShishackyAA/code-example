<?php

namespace frontend\modules\agreementRules\baseModels;

use common\models\BaseModel;
use common\models\DocumentRolesEmployees;
use common\models\Employee;
use frontend\modules\agreementRules\models\CommentsModel;
use frontend\modules\agreementRules\models\FilesModel;
use frontend\modules\agreementRules\models\SignaturesModel;
use frontend\modules\agreementRules\traits\CommonQuery;
use frontend\modules\storage\models\FileModel;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "agreement_rules.rules".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 * @property string|null $type
 * @property string|null $bn_code
 * @property string|null $folder
 * @property string|null $employee_created
 * @property string|null $status
 * @property int|null $version
 * @property int|null $date_publication
 * @property int|null $updated
 * @property int|null $created
 * @property int|null $previous_version
 * @property FilesModel[] $currentFiles
 * @property CommentsModel[] $comments
 * @property SignaturesModel[] $allSignatures
 * @property SignaturesModel[] $rejectSignatures
 * @property SignaturesModel[] $currentSignatures
 * @property Participants[] $participants
 * @property SignaturesModel $currentSignature
 * @property array $employeesForCurrentSignature
 * @property string $number
 * @property string $section
 * @property FileModel $fileStorage
 */
class Rules extends BaseModel
{
    use CommonQuery;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agreement_rules.rules';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number', 'section'], 'string'],
            [['version', 'date_publication', 'updated', 'created'], 'default', 'value' => null],
            [['version', 'date_publication', 'updated', 'created', 'previous_version'], 'integer'],
            [['title', 'type', 'bn_code', 'folder', 'employee_created', 'status'], 'string', 'max' => 255],
            [['number', 'title', 'type', 'bn_code', 'folder'], 'filter', 'filter' => 'trim'],
            [['section', 'title'], 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => \Yii::t('rules', 'Title'),
            'type' => \Yii::t('rules', 'Type'),
            'bn_code' => \Yii::t('rules', 'Bn code'),
            'folder' => \Yii::t('rules', 'Folder'),
            'employee_created' => \Yii::t('rules', 'Employee Created'),
            'status' => \Yii::t('rules', 'Status'),
            'version' => \Yii::t('rules', 'Version'),
            'date_publication' => \Yii::t('rules', 'Date Publication'),
            'updated' => \Yii::t('rules', 'Updated'),
            'created' => \Yii::t('rules', 'Created'),
            'previous_version' => \Yii::t('rules', 'Previous version'),
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'updated'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated'],
                ],
                'value' => time(),
            ],
        ];
    }

    public function getCurrentFiles()
    {
        return $this->hasMany(FilesModel::class, ['rule_id' => 'id'])
            ->orderBy(' version, created DESC');
    }

    public function getFileStorage()
    {
        return $this->hasOne(FileModel::class, ['relation_id' => 'id'])
            ->andWhere(['relation_type' => FileModel::RELATION_TYPE_RULES]);
    }

    public function getComments()
    {
        return $this->hasMany(CommentsModel::class, ['rule_id' => 'id'])
            ->select([
                'comments.*',
                'employee_name' => '"employees"."last_name" || \' \' || "employees"."first_name"',
                'is_self_comment' => "(CASE WHEN employee_code = '" . \Yii::$app->user->id . "' THEN true
                       ELSE false
                    END)"])
            ->leftJoin(Employee::tableName(), 'employees.id = comments.employee_code')
            ->orderBy('created DESC');
    }

    public function getAllSignatures()
    {
        return $this->hasMany(SignaturesModel::class, ['rule_id' => 'id'])
            ->orderBy('version, priority');
    }

    public function getRejectSignatures()
    {
        return $this->hasMany(SignaturesModel::class, ['rule_id' => 'id'])
            ->select([
                'signatures.*',
                'employee_name' => '"employees"."last_name" || \' \' || "employees"."first_name"'
            ])
            ->leftJoin(Employee::tableName(), 'employees.id = signatures.employee_code')
            ->where(['signatures.status' => self::STATUS_REJECTED])
            ->orderBy('signatures.id DESC');
    }

    public function getCurrentSignatures()
    {
        return $this->hasMany(SignaturesModel::class, ['rule_id' => 'id', 'version' => 'version'])
            ->select([
                'signatures.*',
                'employee_name' => '"employees"."last_name" || \' \' || "employees"."first_name"'
            ])
            ->leftJoin(Employee::tableName(), 'employees.id = signatures.employee_code')
            ->orderBy('priority');
    }

    public function getParticipants()
    {
        return $this->hasMany(Participants::class, ['rule_id' => 'id', 'version' => 'version'])
            ->select([
                'participants.*',
                'employee_name' => '"employees"."last_name" || \' \' || "employees"."first_name"',
                'img' => 'employees.comp_employee_photopath'
            ])
            ->leftJoin(Employee::tableName(), 'employees.id = participants.employee_code')
            ->indexBy('employee_code')
            ->orderBy('id');
    }

    public function getCurrentSignature()
    {
        return $this->hasOne(SignaturesModel::class, ['rule_id' => 'id', 'version' => 'version'])
            ->where(['status' => [self::STATUS_ACTIVE, self::STATUS_REJECTED]])
            ->orderBy('priority');
    }

    public function getEmployeesForCurrentSignature()
    {
        $query = $this->hasMany(SignaturesModel::class, ['rule_id' => 'id', 'version' => 'version']);

        return self::queryEmployeesForCurrentSign($query)
            ->asArray();
    }
}
