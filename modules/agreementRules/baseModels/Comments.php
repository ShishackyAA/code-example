<?php

namespace frontend\modules\agreementRules\baseModels;

use common\models\BaseModel;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "agreement_rules.comments".
 *
 * @property int $id
 * @property int|null $rule_id
 * @property string|null $employee_code
 * @property string|null $text
 * @property int|null $version
 * @property string|null $status
 * @property int|null $created
 */
class Comments extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agreement_rules.comments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rule_id', 'version', 'created'], 'default', 'value' => null],
            [['rule_id', 'version', 'created'], 'integer'],
            [['text'], 'string'],
            [['status'], 'default', 'value' => self::STATUS_ACTIVE],
            [['employee_code', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rule_id' => 'Rule ID',
            'employee_code' => \Yii::t('rules', 'Employee'),
            'text' => \Yii::t('rules', 'Text'),
            'version' => \Yii::t('rules', 'Version'),
            'status' => \Yii::t('rules', 'Status'),
            'created' => \Yii::t('rules', 'Created'),
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
                'value' => time(),
            ],
        ];
    }
}
