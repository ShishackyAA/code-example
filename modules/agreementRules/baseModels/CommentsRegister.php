<?php

namespace frontend\modules\agreementRules\baseModels;

use common\models\BaseModel;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "agreement_rules.comments_register".
 *
 * @property int $id
 * @property int|null $rule_id
 * @property string|null $employee_code
 * @property int|null $created
 * @property int|null $updated
 */
class CommentsRegister extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agreement_rules.comments_register';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rule_id', 'created', 'updated'], 'default', 'value' => null],
            [['rule_id', 'created', 'updated'], 'integer'],
            [['employee_code'], 'string', 'max' => 255],
            [['employee_code', 'rule_id'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rule_id' => 'Rule ID',
            'employee_code' => 'Employee Code',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'updated'],
                    ActiveRecord::EVENT_BEFORE_UPDATE=> ['updated'],
                ],
                'value' => time(),
            ],
        ];
    }


}
