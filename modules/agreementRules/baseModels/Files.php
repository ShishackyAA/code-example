<?php

namespace frontend\modules\agreementRules\baseModels;

use common\models\BaseModel;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "agreement_rules.files".
 *
 * @property int $id
 * @property int|null $rule_id
 * @property string|null $name
 * @property string|null $original_name
 * @property string|null $path
 * @property string|null $extension
 * @property string|null $status
 * @property int|null $created
 * @property int|null $version
 */
class Files extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agreement_rules.files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rule_id', 'created'], 'default', 'value' => null],
            [['rule_id', 'created', 'version'], 'integer'],
            [['name', 'original_name', 'path', 'extension', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rule_id' => 'Rule ID',
            'name' => \Yii::t('rules', 'Name'),
            'original_name' => \Yii::t('rules', 'Original Name'),
            'path' => \Yii::t('rules', 'Path'),
            'extension' => \Yii::t('rules', 'Extension'),
            'status' => \Yii::t('rules', 'Status'),
            'created' => 'Created',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
                'value' => time(),
            ],
        ];
    }
}
