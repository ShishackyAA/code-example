<?php

namespace frontend\modules\agreementRules\baseModels;

use common\models\BaseModel;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "agreement_rules.signatures".
 *
 * @property int $id
 * @property int|null $rule_id
 * @property string|null $employee_code
 * @property string|null $role_code
 * @property string|null $role_name
 * @property int|null $date_signed
 * @property int|null $version
 * @property int|null $priority
 * @property string|null $status
 * @property int|null $created
 * @property int|null $updated
 * @property string|null $comment
 */
class Signatures extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agreement_rules.signatures';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rule_id', 'date_signed', 'version', 'created', 'updated'], 'default', 'value' => null],
            [['rule_id', 'date_signed', 'version', 'created', 'updated', 'priority'], 'integer'],
            [['employee_code', 'role_code', 'role_name', 'status'], 'string', 'max' => 255],
            [['comment'], 'string'],
            [['comment', 'employee_code', 'role_code', 'role_name'], 'filter', 'filter' => 'trim'],
            [['employee_code'], 'default', 'value' => null],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rule_id' => 'Rule ID',
            'priority' => 'priority',
            'employee_code' => \Yii::t('rules', 'Employee'),
            'role_code' => 'Role Code',
            'role_name' => \Yii::t('rules', 'Role'),
            'date_signed' => \Yii::t('rules', 'Date Signed'),
            'version' => \Yii::t('rules', 'Version'),
            'status' => \Yii::t('rules', 'Status'),
            'created' => \Yii::t('rules', 'Created'),
            'updated' => \Yii::t('rules', 'Updated'),
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'updated'],
                    ActiveRecord::EVENT_BEFORE_UPDATE=> ['updated'],
                ],
                'value' => time(),
            ],
        ];
    }
}
