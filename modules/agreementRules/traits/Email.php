<?php

namespace frontend\modules\agreementRules\traits;

use common\models\redis\Employee;
use frontend\modules\agreementRules\models\SearchRulesModel;
use frontend\modules\email\models\CommonEmail;

trait Email
{
    public $pathToEmail = '@frontend/modules/agreementRules/views/email/';

    public function messageNeedSignEmployee()
    {
        $rule = SearchRulesModel::getModel($this->rule_id);
        foreach ($this->getEmployeesForSignature() as $employeeCode => $employeeData) {
            $html = \Yii::$app->view->render($this->pathToEmail . 'needSignEmployee', [
                'rule' => $rule,
                'employeeName' => $employeeData['employee_name']
            ]);
            CommonEmail::createModel()
                ->setEmailsTo([
                    $employeeData['employee_email'],
                ])
                ->setTitle('Требуется ваше согласование')
                ->setSubject('Требуется ваше согласование(VENGO)')
                ->setHtmlBody($html)
                ->send();
        }
    }

    public function messageAgreedRule()
    {
        $rule = SearchRulesModel::getModel($this->rule_id);
        $employee = Employee::getEmployee($rule->employee_created);
        $html = \Yii::$app->view->render($this->pathToEmail . 'agreedRule', [
            'rule' => $rule,
            'employeeName' => $employee->full_short_name
        ]);
        CommonEmail::createModel()
            ->setEmailsTo([
                $employee->email,
            ])
            ->setTitle('Заявка согласованна')
            ->setSubject('Заявка согласованна(VENGO)')
            ->setHtmlBody($html)
            ->send();
    }

    public function messageRejectRule()
    {
        $rule = SearchRulesModel::getModel($this->rule_id);
        $employee = Employee::getEmployee($rule->employee_created);
        $html = \Yii::$app->view->render($this->pathToEmail . 'rejectRule', [
            'rule' => $rule,
            'employeeName' => $employee->full_short_name
        ]);
        CommonEmail::createModel()
            ->setEmailsTo([
                $employee->email,
            ])
            ->setTitle('Заявка отказана')
            ->setSubject('Заявка отказана(VENGO)')
            ->setHtmlBody($html)
            ->send();
    }
}
