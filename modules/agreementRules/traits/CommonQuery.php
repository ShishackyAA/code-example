<?php


namespace frontend\modules\agreementRules\traits;


use common\models\DocumentRolesEmployees;
use common\models\Employee;
use frontend\modules\agreementRules\models\RulesModel;
use frontend\modules\agreementRules\models\SignaturesModel;
use yii\db\ActiveQuery;

trait CommonQuery
{
    public static function queryEmployeesForCurrentSign(ActiveQuery $activeQuery): ActiveQuery
    {
        $queryCurrentSignature = SignaturesModel::find()->select(['subQuerySign.id'])
            ->where([
                'subQuerySign.status' => [RulesModel::STATUS_ACTIVE],
            ])
            ->andWhere('"subQuerySign".rule_id = rules.id')
            ->andWhere('"subQuerySign".version = rules.version')
            ->alias('subQuerySign')
            ->orderBy('"subQuerySign".priority')
            ->limit(1);

        return $activeQuery->select([
            'signatures.rule_id',
            'signatures.version',
            'document_roles_employees.employee_code',
            'employee_name' => '"employees"."last_name" || \' \' || "employees"."first_name"',
            'employee_email' => '"employees"."comp_email"',
        ])
            ->leftJoin(DocumentRolesEmployees::tableName(), 'document_roles_employees.role_code 
                = signatures.role_code')
            ->leftJoin(Employee::tableName(), 'employees.id = document_roles_employees.employee_code')
            ->leftJoin(RulesModel::tableName(), 'rules.id = signatures.rule_id')
            ->andWhere('rules.bn_code = document_roles_employees.cfr_filter')
            ->andWhere([
                'signatures.id' => $queryCurrentSignature,
                'rules.status' => [RulesModel::STATUS_APPLICANT_OK, RulesModel::STATUS_SPECIALIST_OK],
                'document_roles_employees.action_canceled' => false
            ])
            ->andWhere("CASE
                  WHEN signatures.employee_code IS NOT NULL
                      THEN document_roles_employees.employee_code = signatures.employee_code
                  ELSE true
            END")
            ->indexBy('employee_code');
    }

    public static function rulesToSign()
    {
        return self::queryEmployeesForCurrentSign(SignaturesModel::find())
            ->andWhere([
                'document_roles_employees.employee_code' => \Yii::$app->user->id
            ]);
    }
}
