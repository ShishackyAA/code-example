<?php

namespace frontend\modules\agreementRules\controllers;

use app\modules\storage\models\FolderModel;
use common\models\Cfr;
use common\models\Employee;
use frontend\modules\agreementRules\baseModels\Participants;
use frontend\modules\agreementRules\models\CommentsModel;
use frontend\modules\agreementRules\models\RulesModel;
use frontend\modules\agreementRules\models\SearchRulesModel;
use frontend\modules\agreementRules\models\SignaturesModel;
use yii\filters\AccessControl;
use \frontend\components\Controller;

class RulesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'load-initial-data',
                            'agreement-rules',
                            'set-tag',
                            'load-data-view',
                            'search-previous-agreed'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \Yii::$app->user->identity->can(RulesModel::ROLE_READ);
                        }
                    ],
                    [
                        'actions' => [
                            'create-instruction',
                            'update',
                            'release',
                            'delete',
                            'cancel',
                            're-release'
                        ],
                        'allow' => true,
                        'verbs' => ['POST'],
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \Yii::$app->user->identity->can(RulesModel::ROLE_CREATE);
                        }
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        \Yii::$app->session->set('activeMenu', 'instructions');
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'modelSearch' => new SearchRulesModel()
        ]);
    }

    public function actionLoadInitialData()
    {
        $searchModel = new SearchRulesModel();
        return $this->vueResponse([
            'rules' => $searchModel->getRulesList(),
            'isCanCreate' => \Yii::$app->user->identity->can(RulesModel::ROLE_CREATE),
            'activeFilters' => $searchModel->activeFilters(),
            'employeeCode' => \Yii::$app->user->id,
            'countToSign' => SearchRulesModel::getCountRulesToSign(),
            'filterSignatures' => SignaturesModel::signaturesName(),
            'tags' => SearchRulesModel::tags(),
        ]);
    }

    public function actionAgreementRules()
    {
        $searchModel = new SearchRulesModel();

        return $this->vueResponse([
            'rules' => $searchModel->getRulesList(),
            'activeFilters' => $searchModel->activeFilters(),
        ]);
    }

    public function actionSetTag()
    {
        SearchRulesModel::setTag();
        $searchModel = new SearchRulesModel();


        return $this->vueResponse([
            'tags' => SearchRulesModel::tags(),
            'rules' => $searchModel->getRulesList()
        ]);
    }

    public function actionCreateInstruction()
    {
        $model = RulesModel::createInstruction(\Yii::$app->request->post());

        return $this->vueResponse([
            'ruleData' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = RulesModel::updateModel($id, \Yii::$app->request->post());

        return $this->vueResponse([
            'ruleData' => $model,
        ]);
    }

    public function actionLoadDataView($id)
    {
        $ruleData = SearchRulesModel::getModel($id);
        $currentSign = $ruleData->currentSignature;

        return $this->vueResponse([
            'isCanCreate' => \Yii::$app->user->identity->can(RulesModel::ROLE_CREATE),
            'isCanApproveSign' => $currentSign ? $currentSign->isCanApproveSign($ruleData->status) : false,
            'isCanAddSign' => $ruleData->isCanAddSignature(),
            'ruleData' => $ruleData,
            'employeeCode' => \Yii::$app->user->id,
            'currentSignature' => $currentSign,
            'comments' => $ruleData->comments,
            'currentSignatures' => $ruleData->currentSignatures,
            'rejectSignatures' => $ruleData->rejectSignatures,
            'files' => $ruleData->currentFiles,
            'participants' => Participants::completeParticipantsList($ruleData->participants),
            'countNewComments' => CommentsModel::getCountNewComments(\Yii::$app->user->id, $ruleData->id),
            'employeesForCurrentSign' => $ruleData->employeesForCurrentSignature,
            'types' => SearchRulesModel::types(),
            'bnCodes' => Cfr::getBusinessLinesVue(),
            'folders' => FolderModel::childFoldersTree(FolderModel::FOLDER_MT),
        ]);
    }

    public function actionRelease($id)
    {
        RulesModel::release($id);

        return $this->returnCommonData($id);
    }

    public function actionCancel($id)
    {
        RulesModel::cancel($id);

        return $this->returnCommonData($id);
    }

    public function actionDelete($id)
    {
        RulesModel::deleteModel($id);
        $searchModel = new SearchRulesModel();

        return $this->vueResponse([
            'rules' => $searchModel->getRulesList(),
            'activeFilters' => $searchModel->activeFilters(),
        ]);
    }

    public function actionReRelease($id)
    {
        RulesModel::reRelease($id);

        return $this->returnCommonData($id);
    }

    public function actionSearchPreviousAgreed()
    {
        $search = trim(\Yii::$app->request->get('search'));
        $out = [];
        if ($search) {
            $out = SearchRulesModel::getPreviousAgreements($search);
        }

        return $this->vueResponse($out);
    }

    private function returnCommonData($id)
    {
        $ruleData = SearchRulesModel::getModel($id);
        $currentSign = $ruleData->currentSignature;

        return $this->vueResponse([
            'isCanCreate' => \Yii::$app->user->identity->can(RulesModel::ROLE_CREATE),
            'isCanApproveSign' => $currentSign ? $currentSign->isCanApproveSign($ruleData->status) : false,
            'isCanAddSign' => $ruleData->isCanAddSignature(),
            'ruleData' => $ruleData,
            'employeeCode' => \Yii::$app->user->id,
            'currentSignature' => $currentSign,
            'currentSignatures' => $ruleData->currentSignatures,
            'rejectSignatures' => $ruleData->rejectSignatures,
            'employeesForCurrentSign' => $ruleData->employeesForCurrentSignature,
            'participants' => Participants::completeParticipantsList($ruleData->participants),
        ]);
    }
}
