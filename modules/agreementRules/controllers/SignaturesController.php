<?php

namespace frontend\modules\agreementRules\controllers;

use frontend\modules\agreementRules\baseModels\Participants;
use frontend\modules\agreementRules\models\RulesModel;
use frontend\modules\agreementRules\models\SearchRulesModel;
use frontend\modules\agreementRules\models\SignaturesModel;
use yii\filters\AccessControl;
use \frontend\components\Controller;

class SignaturesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'add-specialists',
                            'delete',
                            'approve',
                            'reject',
                            'sort'
                        ],
                        'allow' => true,
                        'verbs' => ['POST'],
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \Yii::$app->user->identity->can(RulesModel::ROLE_READ);
                        }
                    ],
                ],
            ],
        ];
    }

    public function actionAddSpecialists($ruleId)
    {
        $ruleData = SignaturesModel::addSignaturesSpecialist($ruleId, \Yii::$app->request->post('employees'));

        return $this->responseData($ruleData);
    }

    public function actionDelete($id)
    {
        $ruleData = SignaturesModel::deleteModel($id);

        return $this->responseData($ruleData);
    }

    public function actionSort($ruleId)
    {
        $ruleData = SignaturesModel::sort($ruleId, \Yii::$app->request->post('signatures'));

        return $this->responseData($ruleData);
    }

    public function actionApprove($id)
    {
        $ruleData = SignaturesModel::approve($id);

        return $this->responseData($ruleData);
    }

    public function actionReject($id)
    {
        $ruleData = SignaturesModel::reject($id);

        return $this->responseData($ruleData);
    }

    /**
     * @param $ruleData SearchRulesModel
     * @return array
     */
    private function responseData($ruleData)
    {
        $currentSign = $ruleData->currentSignature;

        return $this->vueResponse([
            'isCanCreate' => \Yii::$app->user->identity->can(RulesModel::ROLE_CREATE),
            'isCanApproveSign' => $currentSign ? $currentSign->isCanApproveSign($ruleData->status) : false,
            'isCanAddSign' => $ruleData->isCanAddSignature(),
            'ruleData' => $ruleData,
            'currentSignature' => $currentSign,
            'currentSignatures' => $ruleData->currentSignatures,
            'rejectSignatures' => $ruleData->rejectSignatures,
            'employeesForCurrentSign' => $ruleData->employeesForCurrentSignature,
            'participants' => Participants::completeParticipantsList($ruleData->participants),
        ]);
    }
}
