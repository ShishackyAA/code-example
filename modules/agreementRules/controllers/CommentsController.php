<?php

namespace frontend\modules\agreementRules\controllers;


use frontend\modules\agreementRules\models\CommentsModel;
use frontend\modules\agreementRules\models\RulesModel;
use frontend\modules\agreementRules\models\SearchRulesModel;
use yii\filters\AccessControl;
use \frontend\components\Controller;

class CommentsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'add',
                            'delete',
                            'set-view-comments',
                        ],
                        'allow' => true,
                        'verbs' => ['POST', 'GET'],
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \Yii::$app->user->identity->can(RulesModel::ROLE_READ);
                        }
                    ],
                ],
            ],
        ];
    }

    public function actionAdd($ruleId)
    {
        $rule = CommentsModel::addComment($ruleId, \Yii::$app->request->post('comment'));

        return $this->vueResponse([
            'comments' => $rule->comments
        ]);
    }

    public function actionDelete($id)
    {
        $ruleId = CommentsModel::deleteModel($id);

        return $this->vueResponse([
            'comments' => SearchRulesModel::getModel($ruleId)->comments
        ]);
    }

    public function actionSetViewComments($ruleId)
    {
        CommentsModel::setViewComments($ruleId);

        return $this->vueResponse([
            'comments' => SearchRulesModel::getModel($ruleId)->comments
        ]);
    }
}
