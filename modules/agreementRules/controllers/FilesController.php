<?php

namespace frontend\modules\agreementRules\controllers;


use frontend\modules\agreementRules\models\FilesModel;
use frontend\modules\agreementRules\models\RulesModel;
use yii\filters\AccessControl;
use \frontend\components\Controller;

class FilesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'get-url-edit',
                            'download',
                        ],
                        'allow' => true,
                        'verbs' => ['POST', 'GET'],
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \Yii::$app->user->identity->can(RulesModel::ROLE_READ);
                        }
                    ],
                    [
                        'actions' => [
                            'add',
                            'delete',
                        ],
                        'allow' => true,
                        'verbs' => ['POST'],
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \Yii::$app->user->identity->can(RulesModel::ROLE_CREATE);
                        }
                    ],
                ],
            ],
        ];
    }


    public function actionGetUrlEdit($id)
    {
        return $this->vueResponse([
            'url' => FilesModel::getUrlForEdit($id)
        ]);
    }

    public function actionAdd($ruleId) {
        $files = FilesModel::loadFile($ruleId);
        $this->messages = \Yii::t('rules', 'Files was uploaded');
        $this->showMessage = true;

        return $this->vueResponse([
            'files' => $files
        ]);
    }

    public function actionDelete($id) {
        $files = FilesModel::deleteFile($id);
        $this->messages = \Yii::t('rules', 'Files was deleted');
        $this->showMessage = true;

        return $this->vueResponse([
            'files' => $files
        ]);
    }

    public function actionDownload($id)
    {
        return FilesModel::download($id);
    }
}
