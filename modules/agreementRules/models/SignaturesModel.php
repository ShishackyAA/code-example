<?php

namespace frontend\modules\agreementRules\models;

use common\helpers\ArrayHelper;
use common\interfaces\BaseFunction;
use common\models\DocumentRolesEmployees;
use common\models\redis\Employee;
use common\traits\CommonFunctions;
use frontend\components\EmployeeException;
use frontend\modules\agreementRules\baseModels\Participants;
use frontend\modules\agreementRules\baseModels\Signatures;
use frontend\modules\agreementRules\traits\CommonQuery;
use frontend\modules\agreementRules\traits\Email;
use phpDocumentor\Reflection\Types\Self_;

class SignaturesModel extends Signatures implements BaseFunction
{
    use CommonFunctions, CommonQuery, Email;

    const PRIORITY_CREATOR = 1;
    const PRIORITY_EDITOR = 2;

    public $employeesForSign = [];

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['status', 'default', 'value' => self::STATUS_ACTIVE]
        ]);
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'employee_name'
        ]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $this->addParticipantsForSignature();
        }
    }

    public function beforeDelete()
    {
        $this->deleteParticipants();
        return parent::beforeDelete();
    }

    public static function signaturesName()
    {
        return [
            RulesModel::ROLE_EDITOR => \Yii::t('rules', 'Editor'),
            RulesModel::ROLE_CREATE => \Yii::t('rules', 'Creator'),
            RulesModel::ROLE_SPECIALIST => \Yii::t('rules', 'Specialist'),
        ];
    }

    public static function getSignatureName($roleName)
    {
        return isset(self::signaturesName()[$roleName]) ? self::signaturesName()[$roleName] : '';
    }

    public static function addSignaturesSpecialist($ruleId, $newEmployees)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if (!$newEmployees) {
                throw new EmployeeException(\Yii::t('rules', 'Not  employees for add'));
            }
            $ruleModel = SearchRulesModel::getModel($ruleId);
            self::checkCanAction($ruleModel->employee_created);
            $currentSignature = $ruleModel->currentSignature;
            $oldSignatures = $ruleModel->currentSignatures;
            $priority = $currentSignature->addNewSignatures($newEmployees);

            foreach ($oldSignatures as $signature) {
                if ($signature->priority > $currentSignature->priority) {
                    $signature->priority = $priority;
                    $signature->save();
                    $priority++;
                }
            }
            unset($ruleModel->currentSignatures);
            $transaction->commit();

            return $ruleModel;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function deleteModel($id)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $model = self::getModel($id);
            $ruleModel = SearchRulesModel::getModel($model->rule_id);
            self::checkCanAction($ruleModel->employee_created);
            if ($model->role_code != RulesModel::ROLE_SPECIALIST && $model->status === self::STATUS_ACTIVE) {
                throw new EmployeeException(\Yii::t('rules', 'You can deleting only specialist signature'));
            }
            $model->delete();
            self::reorderSignatures($ruleModel->currentSignatures);
            $transaction->commit();

            return $ruleModel;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function sort($ruleId, $sortSignatures)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $priority = 1;
            $ruleModel = SearchRulesModel::getModel($ruleId);
            self::checkCanAction($ruleModel->employee_created);
            $currentSignatures = $ruleModel->getCurrentSignatures()->indexBy('id')->all();
            if (!$sortSignatures) {
                throw new EmployeeException(\Yii::t('rules', 'Not  signatures for sort'));
            }
            foreach ($sortSignatures as $signatureId) {
                if (isset($currentSignatures[$signatureId])) {
                    $signature = $currentSignatures[$signatureId];
                    if ($signature->status == self::STATUS_ACTIVE) {
                        $signature->priority = $priority;
                        $signature->save();
                    }
                }
                $priority++;
            }
            $transaction->commit();

            return $ruleModel;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function approve($id)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $signature = self::getModel($id);
            $ruleModel = RulesModel::getModel($signature->rule_id);
            if ($signature->priority == 1) {
                $ruleModel->status = RulesModel::STATUS_APPLICANT_OK;
            }
            $signature->setDataSignature(self::STATUS_APPROVE, $ruleModel->status);
            $ruleModel->setStatusByRoleSignature($signature->role_code);
            $ruleModel->save();
            if ($ruleModel->status == RulesModel::STATUS_AGREED) {
                $signature->messageAgreedRule();
            } else {
                $ruleModel->currentSignature->messageNeedSignEmployee();
            }
            $transaction->commit();

            return SearchRulesModel::getModel($signature->rule_id);
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function reject($id)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $signature = self::getModel($id);
            $ruleModel = RulesModel::getModel($signature->rule_id);
            $signature->setDataSignature(self::STATUS_REJECTED, $ruleModel->status);

            $ruleModel->status = self::STATUS_REJECTED;
            $ruleModel->save();
            $signature->messageRejectRule();
            $transaction->commit();

            return SearchRulesModel::getModel($signature->rule_id);
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @param $ruleModel RulesModel
     * @param $roleCode
     * @param $priority
     * @param null $employeeCode
     */
    public static function createSignAfterCreateRule($ruleModel, $roleCode, $priority, $employeeCode = null)
    {
        $sign = new SignaturesModel();
        $sign->rule_id = $ruleModel->id;
        $sign->priority = $priority;
        $sign->version = $ruleModel->version;
        $sign->role_code = $roleCode;
        $sign->employee_code = $employeeCode;
        $sign->role_name = $sign->getSignNameByRole();
        $sign->save();
    }

    /**
     * @param SignaturesModel[] $signatures
     */
    public static function reorderSignatures($signatures)
    {
        $priority = 1;
        foreach ($signatures as $signature) {
            $signature->priority = $priority;
            $signature->save();
            $priority++;
        }
    }

    public static function checkCanAction($creator)
    {
        if ($creator != \Yii::$app->user->id) {
            throw new EmployeeException(\Yii::t('rules', 'You not allowed this action'));
        }
    }

    /**
     * @param SignaturesModel[] $signatures
     * @return SignaturesModel[]
     */
    public static function signaturesListComplete($signatures)
    {
        return $signatures;
    }

    public function getSignNameByRole()
    {
        return self::signaturesName()[$this->role_code] ?? self::signaturesName()[$this->role_code];
    }

    public function getEmployeesForSignature()
    {
        if (empty($this->employeesForSign)) {
            if ($this->employee_code) {
                $employee = Employee::getEmployee($this->employee_code);
                $this->employeesForSign[$this->employee_code] = [
                    'employee_code' => $this->employee_code,
                    'employee_name' => $employee->full_name,
                    'employee_email' => $employee->comp_email,
                ];
            }
            if (!$this->employee_code) {
                $this->employeesForSign = self::queryEmployeesForCurrentSign(self::find())
                    ->andWhere(['rules.id' => $this->rule_id])
                    ->asArray()
                    ->all();
            }
        }

        return $this->employeesForSign;
    }

    public function getEmployeesForDeleteSignature()
    {
        $query = self::find()
            ->select([
                'document_roles_employees.id',
                'document_roles_employees.employee_code',
            ])
            ->leftJoin(DocumentRolesEmployees::tableName(), 'document_roles_employees.role_code = signatures.role_code')
            ->where([
                'signatures.rule_id' => $this->rule_id,
                'signatures.version' => $this->version,
            ])
            ->andWhere(['<>', 'signatures.id', $this->id])
            ->andWhere('CASE
              WHEN signatures.employee_code IS NOT NULL
                  THEN document_roles_employees.employee_code = signatures.employee_code
              ELSE true END');

        return ArrayHelper::map($query->asArray()->all(), 'id', 'employee_code');
    }

    public function isCanApproveSign($ruleStatus): bool
    {
        return isset($this->getEmployeesForSignature()[\Yii::$app->user->id])
            && in_array($ruleStatus, [RulesModel::STATUS_APPLICANT_OK, RulesModel::STATUS_SPECIALIST_OK]);
    }

    private function setDataSignature($status, $ruleStatus)
    {
        if (!$this->isCanApproveSign($ruleStatus)) {
            throw new EmployeeException(\Yii::t('rules', 'You dont have access for this signature'));
        }
        $this->date_signed = time();
        $this->status = $status;
        $this->employee_code = \Yii::$app->user->id;
        $this->comment = \Yii::$app->request->post('comment');
        $this->save();
    }

    private function addNewSignatures($newEmployees)
    {
        $priority = $this->priority + 1;
        foreach ($newEmployees as $employee) {
            $signModel = new SignaturesModel();
            $signModel->employee_code = $employee;
            $signModel->rule_id = $this->rule_id;
            $signModel->role_code = RulesModel::ROLE_SPECIALIST;
            $signModel->role_name = $signModel->getSignNameByRole();
            $signModel->version = $this->version;
            $signModel->priority = $priority;
            $signModel->save();
            $priority++;
        }

        return $priority;
    }

    private function addParticipantsForSignature()
    {
        foreach ($this->getEmployeesForSignature() as $employee) {
            if (!$this->isSetParticipant()) {
                $participant = new Participants();
                $participant->employee_code = $employee['employee_code'];
                $participant->rule_id = $this->rule_id;
                $participant->version = $this->version;
                $participant->save();
            }
        }
    }

    private function deleteParticipants()
    {
        $currentEmployees = $this->getEmployeesForDeleteSignature();
        $participants = ArrayHelper::map(SearchRulesModel::getModel($this->rule_id)->participants,
            'id',
            'employee_code');
        $employeeCodes = array_diff($participants, $currentEmployees);
        if (!empty($employeeCodes)) {
            Participants::deleteAll([
                'employee_code' => $employeeCodes,
                'rule_id' => $this->rule_id,
                'version' => $this->version,
            ]);
        }
    }

    private function isSetParticipant()
    {
        return Participants::findOne([
            'employee_code' => $this->employee_code,
            'rule_id' => $this->rule_id,
            'version' => $this->version]) ? true : false;
    }
}
