<?php

namespace frontend\modules\agreementRules\models;

use common\interfaces\BaseFunction;
use common\traits\CommonFunctions;
use frontend\modules\agreementRules\baseModels\Comments;
use frontend\modules\agreementRules\baseModels\CommentsRegister;

class CommentsModel extends Comments implements BaseFunction
{
    use CommonFunctions;

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'employee_name',
            'is_self_comment'
        ]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            self::setViewComments($this->rule_id);
        }
    }

    public static function getCountNewComments($employeeCode, $ruleId)
    {
        $subQueryStr = CommentsRegister::find()
            ->select(['comments_register.updated'])
            ->where("(comments_register.rule_id = comments.rule_id)")
            ->andWhere(['comments_register.employee_code' => $employeeCode])
            ->orderBy('updated DESC')->limit(1)
            ->createCommand()->getRawSql();

        return self::find()
            ->where("comments.created > (coalesce(($subQueryStr), 1))")
            ->andWhere(['comments.rule_id' => $ruleId])
            ->count();
    }

    public static function addComment($ruleId, $text)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $rule = SearchRulesModel::getModel($ruleId);
            $comment = new self();
            $comment->text = $text;
            $comment->employee_code = \Yii::$app->user->id;
            $comment->rule_id = $ruleId;
            $comment->version = $rule->version;
            $comment->save();
            $transaction->commit();

            return $rule;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function deleteModel($id)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $model = self::getModel($id);
            $model->delete();
            $transaction->commit();

            return $model->rule_id;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function setViewComments($ruleId)
    {
        $register = new CommentsRegister();
        $register->rule_id = $ruleId;
        $register->employee_code = \Yii::$app->user->id;
        $register->save();
    }
}