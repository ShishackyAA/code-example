<?php

namespace frontend\modules\agreementRules\models;

use common\interfaces\BaseFunction;
use common\models\Cfr;
use common\traits\CommonFunctions;
use frontend\components\EmployeeException;
use frontend\modules\agreementRules\baseModels\Rules;

class RulesModel extends Rules implements BaseFunction
{
    use CommonFunctions;

    const ROLE_EDIT = 'ARTICLE_EDIT';
    const ROLE_READ = 'ARTICLE_READ';
    const ROLE_CREATE = 'ARTICLE_CREATE';
    const ROLE_EDITOR = 'ARTICLE_EDITOR';
    const ROLE_SPECIALIST = 'ARTICLE_SPECIALIST';

    const QUALITY_DOCUMENTS = 'quality_documents';
    const DOCUMENTS_LABOR_PROTECTION = 'documents_labor_protection';
    const ENVIRONMENT_DOCUMENTS = 'environment_documents';
    const PRODUCTION_INSTRUCTIONS = 'production_instructions';
    const REGULATION_PROCESS = 'regulation_process';
    const COMMON_DOCUMENTS_COMPANY = 'common_documents_company';
    const DOCUMENTS_HR = 'documents_hr';

    const STATUS_APPLICANT_OK = 'applicant_ok';
    const STATUS_SPECIALIST_OK = 'specialist_ok';
    const STATUS_CANCELED = 'canceled';
    const STATUS_AGREED = 'agreed';
    const STATUS_REJECTED = 'rejected';

    const MASK_NUMBER = 'ДОК0000000';
    const SECTION_INSTRUCTION = 'instruction';
    const SECTION_ORDERS = 'orders';

    const SCENARIO_RELEASE_INSTRUCTION = 'release_instruction';
    const SCENARIO_RELEASE_ORDER = 'release_order';

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['title', 'type', 'folder', 'cfr_code'], 'required', 'on' => self::SCENARIO_RELEASE_INSTRUCTION],
            [['title'], 'required', 'on' => self::SCENARIO_CREATE],
            ['employee_created', 'default', 'value' => \Yii::$app->user->id],
            ['status', 'default', 'value' => self::STATUS_NEW],
            ['bn_code', 'default', 'value' => Cfr::CFR_STOREHOUSE],
            ['version', 'default', 'value' => 1],
            ['folder', 'default', 'value' => null],
        ]);
    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_CREATE => $this->attributes(),
            self::SCENARIO_UPDATE => $this->attributes(),
            self::SCENARIO_RELEASE_INSTRUCTION => $this->attributes(),
            self::SCENARIO_RELEASE_ORDER => $this->attributes(),
        ]);
    }

    public function beforeSave($insert)
    {
        if ($this->oldAttributes['previous_version'] != $this->previous_version) {
            $previousRule = SearchRulesModel::getModel($this->previous_version);
            $this->folder = $previousRule->folder;
        }
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $this->addSignCreator();
            $this->addSignEditor();
        }
        if (isset($changedAttributes['version'])) {
            $this->addSignCreator();
            $this->addSignEditor();
        }
        if (isset($changedAttributes['status']) && $this->status == self::STATUS_AGREED) {
            $this->convertFilesAndSaveStorage();
        }
    }

    public function beforeDelete()
    {
        $this->deleteAllComments();
        $this->deleteAllSignatures();
        $this->deleteAllFiles();
        return parent::beforeDelete();
    }

    public static function release($id)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $model = self::getModel($id);
            $model->checkBeforeRelease();
            $model->scenario = $model->section == self::SECTION_INSTRUCTION
                ? self::SCENARIO_RELEASE_INSTRUCTION
                : self::SCENARIO_RELEASE_ORDER;
            $model->save();
            SignaturesModel::approve($model->currentSignature->id);
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function cancel($id)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $model = self::getModel($id);
            $model->checkBeforeCancel();
            $model->status = self::STATUS_CANCELED;
            $model->save();
            $model->rejectSignatureApplicant();
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function deleteModel($id)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $model = self::getModel($id);
            $model->checkBeforeDelete();
            $model->delete();
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function reRelease($id)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $model = self::getModel($id);
            $model->checkBeforeReRelease();
            $model->scenario = self::SCENARIO_CREATE;
            $model->status = self::STATUS_NEW;
            $model->version = $model->version + 1;
            $model->save();
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function types()
    {
        return [
            self::QUALITY_DOCUMENTS => \Yii::t('rules', 'Quality documents'),
            self::DOCUMENTS_LABOR_PROTECTION => \Yii::t('rules', 'Documents labor protection'),
            self::ENVIRONMENT_DOCUMENTS => \Yii::t('rules', 'Environment documents'),
            self::PRODUCTION_INSTRUCTIONS => \Yii::t('rules', 'Production instructions'),
            self::REGULATION_PROCESS => \Yii::t('rules', 'Regulation on process'),
            self::COMMON_DOCUMENTS_COMPANY => \Yii::t('rules', 'Common documents company'),
            self::DOCUMENTS_HR => \Yii::t('rules', 'Documents HR'),
        ];
    }

    public static function createInstruction($postData)
    {
        $model = new self();
        return $model->create($postData, self::SECTION_INSTRUCTION);
    }

    public static function createOrder($postData)
    {
        $model = new self();
        return $model->create($postData, self::SECTION_ORDERS);
    }

    public static function updateModel($id, $postData)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $model = self::getModel($id);
            if ($model->status != self::STATUS_NEW) {
                throw new EmployeeException(\Yii::t('rules',
                    'You can only edit fields in the new status'));
            }
            $model->load($postData, '');
            $model->save();
            $transaction->commit();

            return SearchRulesModel::getModel($id);
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public function setStatusByRoleSignature(string $roleCode)
    {
        switch ($roleCode) {
            case self::ROLE_CREATE:
                $this->status = self::STATUS_APPLICANT_OK;
                break;
            case self::ROLE_SPECIALIST:
                $this->status = self::STATUS_SPECIALIST_OK;
                break;
            case self::ROLE_EDITOR:
                $this->status = self::STATUS_AGREED;
                break;
            default:
                throw new \Exception('Status for rule by role not found for current signature');
        }
    }

    public function isCanAddSignature()
    {
        return $this->employee_created == \Yii::$app->user->id && $this->status != self::STATUS_AGREED;
    }

    private function create(array $postData, string $type): self
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $model = new self();
            $model->load($postData, '');
            $model->scenario = self::SCENARIO_CREATE;
            $model->section = $type;
            $model->save();
            $model->setNumber();
            $transaction->commit();

            return $model;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    private function addSignCreator()
    {
        SignaturesModel::createSignAfterCreateRule($this,
            self::ROLE_CREATE,
            SignaturesModel::PRIORITY_CREATOR,
            \Yii::$app->user->id);
    }

    private function addSignEditor()
    {
        SignaturesModel::createSignAfterCreateRule($this,
            self::ROLE_EDITOR, SignaturesModel::PRIORITY_EDITOR);
    }

    private function checkBeforeRelease()
    {
        if (empty($this->currentFiles)) {
            throw new EmployeeException(\Yii::t('rules', 'You must add files for agreement'));
        }
    }

    private function checkBeforeCancel()
    {
        if (in_array($this->status, [self::STATUS_CANCELED,
            self::STATUS_REJECTED,
            self::STATUS_AGREED])) {
            throw new EmployeeException(\Yii::t('rules', 'Agreement cant canceled'));
        }
    }

    private function checkBeforeDelete()
    {
        if ($this->status != self::STATUS_NEW || $this->version != 1) {
            throw new EmployeeException(\Yii::t('rules', 'Agreement cant deleted'));
        }
    }

    private function checkBeforeReRelease()
    {
        if (!in_array($this->status, [self::STATUS_CANCELED,
            self::STATUS_REJECTED])) {
            throw new EmployeeException(\Yii::t('rules', 'Agreement cant re release'));
        }
    }

    private function setNumber()
    {
        if ($this->id) {
            $this->number = mb_substr(self::MASK_NUMBER, 0, (mb_strlen(self::MASK_NUMBER) - mb_strlen($this->id)))
                . $this->id;
            $this->save();
        }
    }

    private function deleteAllComments()
    {
        CommentsModel::deleteAll([
            'rule_id' => $this->id
        ]);
    }

    private function deleteAllSignatures()
    {
        SignaturesModel::deleteAll([
            'rule_id' => $this->id
        ]);
    }

    private function deleteAllFiles()
    {
        foreach ($this->currentFiles as $currentFile) {
            FilesModel::deleteFile($currentFile->id);
        }
    }

    private function rejectSignatureApplicant()
    {
        foreach ($this->currentSignatures as $currentSignature) {
            $currentSignature->status = self::STATUS_REJECTED;
            $currentSignature->save();
            break;
        }
    }

    private function convertFilesAndSaveStorage()
    {
        if (!$this->currentFiles) {
            throw new EmployeeException(\Yii::t('rules', 'Missing files for save to storage'));
        }
        if ($this->folder) {
            foreach ($this->currentFiles as $file) {
                $file->saveFileInStorageAndConvert($this);
            }
        }
    }
}
