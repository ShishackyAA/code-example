<?php

namespace frontend\modules\agreementRules\models;

use common\interfaces\BaseFunction;
use common\models\UploadForm;
use common\traits\WebDav;
use frontend\components\EmployeeException;
use frontend\modules\agreementRules\baseModels\Files;
use frontend\modules\archive\traits\CommonFunctions;
use frontend\modules\storage\models\FileModel;
use yii\helpers\FileHelper;
use yii\httpclient\Client;
use yii\rbac\Rule;
use yii\web\UploadedFile;

class FilesModel extends Files implements BaseFunction
{
    use WebDav, CommonFunctions;

    const PATH_SAVE_FILES = 'uploads/VengoVision/Storage/Articles';
    const PDF_EXT = 'pdf';
    const URL_TO_CONVERT = 'http://172.20.0.6:3000/';

    public $files;

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['status', 'default', 'value' => self::STATUS_ACTIVE]
        ]);
    }

    public static function getUrlForEdit($id)
    {
        $file = self::getModel($id);

//        $filePath = FileHelper::normalizePath($file->path) . '/';
//        return self::getUrlForWebDav($filePath, $file->name, $file->extension);

        return $file->urlEditFireFox();
    }

    public static function loadFile($ruleId)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $file = UploadedFile::getInstanceByName(self::getModelShortName());
            if (!$file) {
                throw new EmployeeException(\Yii::t('rules', 'No files for adding'));
            }
            $rule = SearchRulesModel::getModel($ruleId);
            if ($rule->status != self::STATUS_NEW) {
                throw new EmployeeException(\Yii::t('rules', 'You cant add this file.
                Agreement status is not new'));
            }
            $uploadForm = new UploadForm();
            $uploadForm->filePath = self::PATH_SAVE_FILES;
            $uploadForm->imageFile = $file;
            $filesModel = new self();
            $filesModel->rule_id = $ruleId;
            $filesModel->path = $uploadForm->getPathForSaveFile();
            $filesModel->original_name = $uploadForm->imageFile->name;
            $filesModel->extension = $uploadForm->imageFile->extension;
            $filesModel->version = $rule->version;
            $filesModel->name = uniqid();
            if (!$file->extension) {
                throw new EmployeeException(\Yii::t('rules', 'Missing file extension'));
            }
            $filesModel->save();
            if (!$uploadForm->uploadFile($filesModel->name)) {
                throw new EmployeeException(\Yii::t('rules', 'File not upload in server'));
            }
            $transaction->commit();

            return $rule->currentFiles;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function deleteFile($fileId)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $file = self::getModel($fileId);
            $rule = SearchRulesModel::getModel($file->rule_id);
            if ($rule->status != self::STATUS_NEW) {
                throw new EmployeeException(\Yii::t('rules', 'You cant delete this file.
                Agreement status is not new'));
            }
            if ($rule->employee_created != \Yii::$app->user->id) {
                throw new EmployeeException(\Yii::t('rules', 'You cant delete this file.
                You must be the Creator'));
            }
            if (file_exists($file->getPath())) {
                unlink($file->getPath());
            }
            $file->delete();
            $transaction->commit();

            return $rule->currentFiles;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function download($fileId)
    {
        $file = self::findOne($fileId);
        if (!$file) {
            throw new EmployeeException(\Yii::t('complaints', 'File not found'));
        }
        $filePath = $file->path . $file->name . '.' . $file->extension;
        $alias = \Yii::getAlias('@frontend/web/' . $filePath);
        if (!file_exists($alias)) {
            throw new EmployeeException(\Yii::t('app', 'File not found in server'));
        }
        if ($file->extension == self::PDF_EXT) {
            return \Yii::$app->response->sendFile($alias, $file->original_name
                . '.' . $file->extension, ['inline' => true]);
        }

        return \Yii::$app->response->sendFile($alias);
    }

    public function getPath()
    {
        return $this->path . $this->name . '.' . $this->extension;
    }

    public function saveFileInStorageAndConvert(RulesModel $rule): bool
    {
        $uploadForm = new UploadForm();
        $uploadForm->group = 'storage/files';
        $typeFile = $this->canConvertFile() ? 'pdf' : $this->extension;
        $filePath = $uploadForm->getPathForSaveFile() . 'convert_file_' . uniqid() . '.' . $typeFile;
        $fileModel = new FileModel();
        if ($rule->previous_version) {
            $previousVersion = SearchRulesModel::getModel($rule->previous_version);
            if ($previousVersion->fileStorage) {
                $fileModel->version = $previousVersion->fileStorage->version + 1;
                $fileModel->number_file = $previousVersion->fileStorage->number_file;
                $oldFile = $previousVersion->fileStorage;
                $oldFile->status = FileModel::STATUS_OLD;
                $oldFile->save();
            }
        }
        $fileModel->file_path = $filePath;
        $fileModel->name = $rule->title;
        $fileModel->relation_id = $rule->id;
        $fileModel->relation_type = FileModel::RELATION_TYPE_RULES;
        $fileModel->extension_code = $typeFile;
        $fileModel->folder_id = $rule->folder;
        $fileModel->save();
        if ($this->canConvertFile()) {
            $this->convertWordToPdfForStorage($filePath);
        } else {
            $isCreateDir = $uploadForm->createDirectory($uploadForm->getPathForSaveFile());
            if ($isCreateDir && !copy($this->getPath(), $filePath)) {
                throw new EmployeeException('File not copy - ' . $this->original_name);
            }
        }

        return  $filePath;
    }

    public function convertWordToPdfForStorage(string $pathToSave): bool
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setFormat(Client::FORMAT_JSON)
            ->setUrl(self::URL_TO_CONVERT)
            ->setData([
                'toType' => 'pdf',
                'fileNameConvert' => $this->getPath(),
                'pathToSave' => $pathToSave
            ])
            ->send();
        if ($response->statusCode != 200) {
            if (isset($response->data['message'])) {
                throw new \Exception($response->data['message']);
            } else {
                throw new \Exception('Unknown error convert document');
            }
        }

        return true;
    }

    private function urlEditFireFox()
    {
        $filePath = str_replace('uploads/', "", $this->path);

        return 'file://///tlh.local/edoc/' . $filePath . $this->name . '.' . $this->extension;
    }

    private function canConvertFile()
    {
        return in_array($this->extension, ['doc', 'docx', 'ppt', 'pptx']);
    }
}
