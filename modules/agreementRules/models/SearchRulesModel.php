<?php

namespace frontend\modules\agreementRules\models;

use common\models\Cfr;
use common\models\redis\Employee;
use \common\models\Employee as EmployeeBase;
use frontend\modules\settings\models\EmployeeSettings;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
 * Class SearchRulesModel
 * @property string $employee_created_name
 * @property string $date_created_str
 */
class SearchRulesModel extends RulesModel
{
    const PAGE_LIMIT = 30;

    public $filterEmployeeCreated;
    public $filterDateStartCreated;
    public $filterDateEndCreated;
    public $filterDateStartPublication;
    public $filterDateEndPublication;
    public $filterTitle;
    public $filterSignature;
    public $filterToSign;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [[
                'filterDateStartCreated',
                'filterDateEndCreated',
                'filterDateStartPublication',
                'filterDateEndPublication',
                'filterTitle',
                'filterSignature',
                'filterEmployeeCreated'], 'string'],
            [['filterToSign'], 'boolean']
        ]);
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'employee_created_name',
            'date_created_str',
            'previous_version_title'
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'filterDateStartCreated' => \Yii::t('rules', 'Start'),
            'filterDateEndCreated' => \Yii::t('rules', 'End'),
            'filterDateStartPublication' => \Yii::t('rules', 'Start'),
            'filterDateEndPublication' => \Yii::t('rules', 'End'),
            'filterTitle' => \Yii::t('rules', 'Title'),
            'filterEmployeeCreated' => \Yii::t('rules', 'Employee created'),
            'filterSignature' => \Yii::t('rules', 'Current signature'),
            'filterToSign' => \Yii::t('rules', 'To sign'),
        ]);
    }

    public static function tags()
    {
        $currentTags = Json::decode(\Yii::$app->user->identity->settings->agreementRulesTags);
        $allTags = [
            self::STATUS_NEW => false,
            self::STATUS_APPLICANT_OK => false,
            self::STATUS_SPECIALIST_OK => false,
            self::STATUS_AGREED => false,
            self::STATUS_REJECTED => false,
            self::STATUS_CANCELED => false,
        ];
        if ($currentTags) {
            foreach ($currentTags as $currentTag) {
                if (isset($allTags[$currentTag])) {
                    $allTags[$currentTag] = true;
                }
            }
        }

        return $allTags;
    }

    public static function setTag()
    {
        $tag = \Yii::$app->request->get('tag');
        if ($tag) {
            $currentTags = Json::decode(\Yii::$app->user->identity->settings->agreementRulesTags);
            if (is_array($currentTags) && in_array($tag, $currentTags)) {
                $key = array_search($tag, $currentTags);
                unset($currentTags[$key]);
            } else {
                $currentTags[] = $tag;
            }
            EmployeeSettings::changeSettings(['agreementRulesTags' => Json::encode($currentTags)]);
        }
    }

    /**
     * @param $id
     * @return self|null
     * @throws NotFoundHttpException
     */
    public static function getModel($id)
    {
        $availableCfrList = \Yii::$app->user->identity->getAvailableCfrForRole(self::ROLE_READ);
        $model = self::find()
            ->select([
                'rules.*',
                'previous_version_title' => 'previous.title'
            ])
            ->leftJoin(self::tableName() . ' AS previous', 'previous.id = rules.previous_version')
            ->where(['rules.id' => $id])
            ->andWhere(['or ilike', 'rules.bn_code', $availableCfrList])
            ->one();
        if (!$model) {
            throw new NotFoundHttpException(\Yii::t('rules', 'Sorry, this agreement not found'));
        }
        $model->employee_created_name = Employee::getEmployeeShotName($model->employee_created);
        $model->date_created_str = date('d-m-Y H:i', $model->created);

        return $model;
    }

    public static function getCountRulesToSign()
    {
        return self::rulesToSign()->count();
    }

    public static function getPreviousAgreements(string $search): array
    {
        $search = trim($search);
        return self::find()
            ->select(['id', 'label' => 'title'])
            ->where([
                'status' => self::STATUS_AGREED
            ])
            ->andWhere(['OR',
                ['ilike', 'number', $search],
                ['ilike', 'title', $search]
            ])
            ->asArray()->orderBy('id DESC')->all();
    }

    public function getRulesList()
    {
        $currentTags = Json::decode(\Yii::$app->user->identity->settings->agreementRulesTags);
        $availableCfrList = \Yii::$app->user->identity->getAvailableCfrForRole(self::ROLE_READ);
        $this->setFilters();
        $rulesQuery = self::find()
            ->select([
                'rules.*',
                'employee_created_name' => '(employees.last_name || \' \' || employees.first_name)',
                'bn_name' => 'cfr.description'
            ])
            ->leftJoin(EmployeeBase::tableName(), 'employees.user_id = rules.employee_created')
            ->leftJoin(Cfr::tableName(), 'cfr.id = rules.bn_code')
            ->filterWhere([
                'employee_created' => $this->filterEmployeeCreated,
            ]);
        $rulesQuery->andWhere(['or ilike', 'rules.bn_code', $availableCfrList]);
        $rulesQuery->andFilterWhere(['ilike', 'title', $this->filterTitle]);
        $rulesQuery->andFilterWhere(['rules.status' => $currentTags]);

        $rulesQuery = $this->setFiltersDate($rulesQuery);


        if ($this->filterSignature) {
            $rulesQuery = $this->filterBySignature($rulesQuery);
        }
        if ($this->filterToSign) {
            $rules = self::rulesToSign()->indexBy('rule_id')->asArray()->all();
            $rulesQuery->andWhere(['rules.id' => array_keys($rules)]);
        }

        $currentPage = \Yii::$app->request->get('page');
        $rulesList = $rulesQuery->orderBy('created DESC')
            ->limit(self::PAGE_LIMIT)->offset(self::PAGE_LIMIT * $currentPage)
            ->with(['currentSignature', 'employeesForCurrentSignature'])
            ->asArray()->all();

        return $this->completeList($rulesList);
    }

    public function setFilters()
    {
        $this->load(\Yii::$app->request->get(), '');
        $redisFilters = EmployeeSettings::getAgreementRulesFilters();
        $activeFilters = $this->activeFilters();
        $currentFilters = [];
        foreach ($activeFilters as $attributeName => $value) {
            if ($this->$attributeName === null) {
                $this->$attributeName = isset($redisFilters[$attributeName]) ? $redisFilters[$attributeName] : '';
            }
            $currentFilters[$attributeName] = $this->$attributeName;
        }
        EmployeeSettings::changeSettings(['agreementRulesFilters' => serialize($currentFilters)]);
    }

    public function activeFilters()
    {
        return [
            'filterEmployeeCreated' => [
                'name' => $this->getAttributeLabel('filterEmployeeCreated'),
                'value' => Employee::getEmployeeShotName($this->filterEmployeeCreated)
            ],
            'filterDateStartCreated' => [
                'name' => $this->getAttributeLabel('filterDateStartCreated'),
                'value' => $this->filterDateStartCreated
            ],
            'filterDateEndCreated' => [
                'name' => $this->getAttributeLabel('filterDateEndCreated'),
                'value' => $this->filterDateEndCreated
            ],
            'filterDateStartPublication' => [
                'name' => $this->getAttributeLabel('filterDateStartPublication'),
                'value' => $this->filterDateStartPublication
            ],
            'filterDateEndPublication' => [
                'name' => $this->getAttributeLabel('filterDateEndPublication'),
                'value' => $this->filterDateEndPublication
            ],
            'filterTitle' => [
                'name' => $this->getAttributeLabel('filterTitle'),
                'value' => $this->filterTitle
            ],
            'filterSignature' => [
                'name' => $this->getAttributeLabel('filterSignature'),
                'value' => SignaturesModel::getSignatureName($this->filterSignature)
            ],
            'filterToSign' => [
                'name' => $this->getAttributeLabel('filterToSign'),
                'value' => $this->filterToSign
            ]
        ];
    }

    private static function isCanApproveSignature($ruleDta)
    {
        return isset($ruleDta['employeesForCurrentSignature'][\Yii::$app->user->id])
            && !in_array($ruleDta['status'], [self::STATUS_REJECTED, self::STATUS_AGREED]);
    }

    /**
     * @param $rulesQuery ActiveQuery
     * @return mixed
     */
    private function setFiltersDate($rulesQuery)
    {
        $filterDateStartCreated = $this->filterDateStartCreated ? strtotime($this->filterDateStartCreated) : null;
        $filterDateEndCreated = $this->filterDateEndCreated ? strtotime($this->filterDateEndCreated) : null;
        $rulesQuery->andFilterWhere(['>=', 'created', $filterDateStartCreated]);
        $rulesQuery->andFilterWhere(['<=', 'created', $filterDateEndCreated]);

        $filterDateStartPublication = $this->filterDateStartPublication
            ? strtotime($this->filterDateStartPublication) : null;
        $filterDateEndPublication = $this->filterDateEndPublication ? strtotime($this->filterDateEndPublication) : null;
        $rulesQuery->andFilterWhere(['>=', 'date_publication', $filterDateStartPublication]);
        $rulesQuery->andFilterWhere(['<=', 'date_publication', $filterDateEndPublication]);

        return $rulesQuery;
    }

    /**
     * @param $rulesQuery ActiveQuery
     * @return mixed
     */
    private function filterBySignature($rulesQuery)
    {
        $queryCurrentSignature = SignaturesModel::find()->select(['subQuerySign.role_code'])
            ->where([
                'subQuerySign.status' => [self::STATUS_ACTIVE, self::STATUS_REJECTED],
            ])
            ->andWhere('"subQuerySign".rule_id = rules.id')
            ->andWhere(['rules.status' => [self::STATUS_SPECIALIST_OK, self::STATUS_APPLICANT_OK]])
            ->andWhere('"subQuerySign".version = rules.version')
            ->alias('subQuerySign')
            ->orderBy('"subQuerySign".priority')
            ->limit(1)->createCommand()->getRawSql();
        $rulesQuery->andWhere('\'' . $this->filterSignature
            . '\' = (' . $queryCurrentSignature . ')');

        return $rulesQuery;
    }

    private function completeList($rulesList)
    {
        for ($i = 0; $i < count($rulesList); $i++) {
            if ($rulesList[$i]['currentSignature']) {
                $currentSignature = $rulesList[$i]['currentSignature'];
                if ($rulesList[$i]['currentSignature']['role_code'] == self::ROLE_SPECIALIST) {
                    if (isset($rulesList[$i]['employeesForCurrentSignature'][$currentSignature['employee_code']])) {
                        $employee = $rulesList[$i]['employeesForCurrentSignature'][$currentSignature['employee_code']];
                        $rulesList[$i]['employeesForCurrentSignature'] = [];
                        $rulesList[$i]['employeesForCurrentSignature'][] = $employee;
                    } else {
                        $rulesList[$i]['employeesForCurrentSignature'] = [];
                    }
                    $rulesList[$i]['currentSignature']['isCanApproveSign'] = self::isCanApproveSignature($rulesList[$i]);
                } else {
                    $rulesList[$i]['currentSignature']['isCanApproveSign'] = self::isCanApproveSignature($rulesList[$i]);
                }

            }
        }

        return $rulesList;
    }
}
