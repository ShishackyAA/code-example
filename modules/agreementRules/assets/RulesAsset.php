<?php

namespace app\modules\agreementRules\assets;

use yii\web\AssetBundle;

class RulesAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/agreementRules/assets/js';

    public $css = [

    ];

    public $js = [
        'agreementRules.js'
    ];

    public $depends = [
        'frontend\assets\AppAsset'
    ];
}
