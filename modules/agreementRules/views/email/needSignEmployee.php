<?php

use \yii\helpers\Url,
    \frontend\modules\agreementRules\models\SearchRulesModel;

/** @var SearchRulesModel $rule */
/** @var string $employeeName */

?>
<p>Добрый день <?= $employeeName ?></p>
<p>Требуется ваше согласование для инструкции - <b><?= $rule->title ?></b>.</p>
<p>
    Ссылка на заявку <b><?= $rule->title ?></b>:
    <a href="<?= Yii::$app->params['host_name'] . Url::to(['/rules/view', 'id' => $rule->id]) ?>">
        Ссылка
    </a>
</p>
