
import Vue from 'vue';
import VueI18n from 'vue-i18n';
import CommonMessages from "../../../vue/messages/common";
import Routing from "../../../vue/js/ParsingUrl";
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';

Vue.use(VueI18n);
Vue.use(Loading);

const i18n = new VueI18n({
    locale: 'ru',
    CommonMessages,
});

const app = new Vue({
    el: '#agreement-rules',
    data: {
        language: 'en',
        currentRoute: window.location.pathname,
        urlParams: [],
        loader: {}
    },
    computed: {
        ViewComponent () {
            let routing = Routing.getComponentByUrl(this.currentRoute);
            this.urlParams = routing.getParamsCurrentUrl();

            return routing.component;
        }
    },
    methods: {
        setLoader: function (containerName) {
            this.loader = this.$loading.show({
                container: containerName,
                canCancel: false,
                color: '#007bff',
            });
        },
        hideLoader: function () {
            this.loader.hide();
        }
    },
    i18n,
    render (h) {
        return h(this.ViewComponent)
    }
});

window.addEventListener('popstate', () => {
    app.currentRoute = window.location.pathname
});