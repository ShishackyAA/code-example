<?php

use app\modules\agreementRules\assets\RulesAsset;
use frontend\modules\agreementRules\models\SearchRulesModel;
use yii\web\View,
    yii\data\ActiveDataProvider;

/** @var $this View */
/** @var $modelSearch SearchRulesModel */
/** @var $dataProvider ActiveDataProvider */

$this->title = Yii::t('rules', 'Agreement rules');
$this->params['breadcrumbs'][] = ['label' => $this->title];

RulesAsset::register($this);
?>
<div class="row">
    <div class="col-md-12">
        <div id="agreement-rules"></div>
    </div>
</div>
