export default {
    en: {
        messages: {
            new: 'New',
            applicant_ok: 'Applicant-OK',
            specialist_ok: 'Specialist-OK',
            agreed: 'Agreed',
            rejected: 'Rejected',
            canceled: 'Canceled',
            deleted: 'Deleted',
            page_not_found: 'Page not found',
            page_not_found_message: 'Sorry, but the page you are looking for is not found.\n' +
                'Try checking the URL FOR errors, then click the refresh button in your browser or return to the',
            home_page: 'home page'
        }
    },
    ru: {
        messages: {
            new: 'Новый',
            applicant_ok: 'Заявитель-ОК',
            specialist_ok: 'Специалист-ОК',
            agreed: 'Согласована',
            rejected: 'Отказана',
            canceled: 'Отменено',
            deleted: 'Удалено',
            page_not_found: 'Страница не найдена',
            page_not_found_message: 'Извините, но страница, которую вы ищете не найдена.\n' +
                'Попробуйте проверить URL-адрес на наличие ошибок, затем нажмите кнопку обновления в браузере или вернитесь на',
            home_page: 'главную страницу'
        }
    }
}