import axios from 'axios';

export default {
    url: '',
    params: {},
    ajax: function (url, params = {}) {
        let obj = this;
        return new Promise(function (resolve, reject) {
            axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
            axios.get(url, {
                params: params,
            }).then(({data}) => obj.handlerResponse(data, resolve))
                .catch(({error}) => obj.catchFunction(error));
        });

    },
    ajaxPost: function (url, params = {}, config = {}) {
        let obj = this;
        let data = new FormData();
        for(let key in params) {
            data.append(key, params[key]);
        }
        return new Promise(function (resolve, reject) {
            axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
            axios.defaults.headers.common['Content-Type'] = 'application/json';
            axios.post(url, data, config)
                .then(({data}) => obj.handlerResponse(data, resolve))
                .catch(({error}) => obj.catchFunction(error));
        });

    },
    handlerResponse: function (data, resolve) {
        if (typeof data === 'object') {
            if (typeof data === 'string') {
                showToast('error', 'Response date return format string!');
            } else {
                if (data.success) {
                    if (data.showMessage && data.messages) {
                        showToast('success', data.messages);
                    }
                    resolve(data);
                } else {
                    showToast('error', data.messages);
                }
            }
        } else {
            showToast('error', 'AxiosWraper - Data is empty :(');
        }
        $('.vld-overlay').hide();
    },
    catchFunction: function (error) {
        console.log(error);
        if (typeof error.response === 'undefined') {
            if (error.response.status === 302) {
                showToast('error', 'You not authorized ' + error);
                window.location.href = "/";
            }
            if (error.response.status === 404) {
                window.location.href = "/not-found";
            } else {
                showToast('error', 'Not connect to API ' + error);
            }
        } else {
            showToast('error', 'Something error ' + error);
        }
    }
}