export default {
    convertToArrayForSelect: function (object) {
        let arr = [];
        if (typeof object === 'object') {
            for (let key in object) {
                arr.push({
                    id: key,
                    label: object[key]
                });
            }
        } else {
            arr = object;
        }

        return arr;
    }
}