export default {
    convertDateWithTime: function (timeStamp) {
        let dateStr = '';
        if (timeStamp) {
            let date = new Date(timeStamp * 1000),
                options = {
                    year: 'numeric',
                    month: 'numeric',
                    day: 'numeric',
                    hour: 'numeric',
                    minute: 'numeric',
                    timezone: 'UTC',
                };

            dateStr = date.toLocaleString("ru", options);
        }

        return dateStr;
    },
    convertDateWithoutTime: function (timeStamp) {
        let dateStr = '';
        if (timeStamp) {
            let date = new Date(timeStamp * 1000),
                options = {
                    year: 'numeric',
                    month: 'numeric',
                    day: 'numeric',
                    timezone: 'UTC',
                };
            dateStr = date.toLocaleString("ru", options);
        }

        return dateStr;
    }
}