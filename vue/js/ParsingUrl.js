import routes from "../routes";

export default {
    currentParams: [],
    routingParams: [],
    params: {},
    component: {},
    getComponentByUrl: function (currentUrl) {
        this.component = routes['/notfound'].component;
        for (let url in routes) {
            if (currentUrl.indexOf(url) === 0) {
                let paramsStr = currentUrl.slice(url.length);
                if (paramsStr) {
                    let currentParams = paramsStr.toString().split('/');
                    if (currentParams.length === routes[url].params.length) {
                        this.currentParams = currentParams;
                        this.routingParams = routes[url].params;
                        this.component = routes[url].component;
                    }
                } else {
                    if (routes[url].params.length === 0) {
                        this.component = routes[url].component;
                    }
                }
                this.urlWithoutParams = url;
            }
        }

        return this;
    },
    getParamsCurrentUrl: function () {
        for (let index in this.routingParams) {
            this.params[this.routingParams[index]] = this.currentParams[index];
        }

        return this.params;
    },
    getUrlWithoutParams: function (currentUrl) {
        let urlWithoutParams = '/notfound';
        for (let url in routes) {
            if (currentUrl.indexOf(url) === 0) {
                urlWithoutParams = url;
            }
        }
        return urlWithoutParams;
    }
}