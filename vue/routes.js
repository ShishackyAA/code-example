import AgreementList from '../modules/agreementRules/views/pages/index/AgreementRules';
import ViewAgreementRule from '../modules/agreementRules/views/pages/view/ViewAgreementRule';
import NotFound from './components/NotFound'

export default {
    '/rules': {
        component: AgreementList,
        params: [],
        title: 'Agreement rules list'
    },
    '/rules/view/': {
        component: ViewAgreementRule,
        params: ['id'],
        title: 'View agreement rule'
    },
    '/notfound': {
        component: NotFound,
        params: ['id'],
        title: 'Page not found'
    }
}