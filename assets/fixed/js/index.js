$(document).ready(function () {
    var allContent = $('.all-content');
    var beginFixed = new StickyElement(allContent);
});
var StickyElement = function(allContent){
    var doc = $(document),
        fixed = false,
        content = allContent.find('.fixed-content'),
        anchor = allContent.find('.anchor');
    var onScroll = function(e){
        var docTop = doc.scrollTop(),
            anchorTop = anchor.offset().top;
        if(docTop > anchorTop - 60){
            if(!fixed){
                anchor.height(content.outerHeight());
                content.addClass('fixed');
                fixed = true;
            }
        }  else   {
            if(fixed){
                anchor.height(0);
                content.removeClass('fixed');
                fixed = false;
            }
        }
    };
    $(window).on('scroll', onScroll);
};