<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class FixedAsset extends AssetBundle
{
    public $sourcePath = '@frontend/assets/fixed';

    public $css = [
        'css/index.css'
    ];

    public $js = [
        'js/index.js',
    ];

    public $depends = [
        'yii\web\YiiAsset'
    ];
}