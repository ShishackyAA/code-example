$(document).ready(function () {
    $(document).on('change', '.filter-list', actionFilterList);
    $(document).on('click', '#navigation-subtabs-rating a', getRatingContent);
    $(document).on('click', 'button.button-action', buttonAction);
    $(document).on('ifChanged', '.icheckRatingList', showActionButtons);
    $(document).on('click', 'a.group-view', showGroup);
    $(document).on('click', 'a.delete-item', deleteItem);
});

function getRatingContent(e) {
    e.preventDefault();
    var url = $(this).data('url');
    document.location.href = url;
}

function actionFilterList() {
    helper.typeForm = 'get';
    helper.isPushUrl = true;
    var idForm = 'filter-list',
        container = '#content-rating-employee';
    helper.afterAction = function () {
        $('[data-toggle="popover"]').popover();
    };
    helper.sendFilterForm(idForm, false, container);
}

function showActionButtons() {
    var element = $('#actionButtons'),
        isHide = true,
        status = $(this).data('status');
    if (!status || status != 'release') {
        $('#list-gridview input:checkbox:checked').each(function () {
            isHide = false;
        });
        if (isHide) {
            element.hide();
        } else {
            element.show();
        }
    }
}

function buttonAction(e) {
    e.preventDefault();
    var arrId = {};
    var checkBox = $('#list-gridview input:checkbox:checked');
    checkBox.each(function () {
        var id = $(this).data('id');
        arrId[id] = id;
    });
    var url = $(this).data('url'),
        interview = $(this).data('interview'),
        interviewCode = $(this).data('interview_code'),
        data = {ids: arrId, RatingSearch: {interview: interview, interview_code: interviewCode}},
        container = '#content-rating-employee';
    helper.isScroll = false;
    helper.typeForm = 'post';
    helper.isPushUrl = false;
    helper.sendServer(url, data, true, container);
}

function showGroup(e) {
    e.preventDefault();
    if ($(this).data('minus') == 1) {
        $(this).find('span').attr('class', 'glyphicon glyphicon-minus');
        $(this).data('minus', '0');
    } else {
        $(this).find('span').attr('class', 'glyphicon glyphicon-plus');
        $(this).data('minus', '1');
    }
    var parentId = $(this).data('id');
    $('tr.parent-id_' + parentId).each(function (i, element) {
        $(this).toggle();
    });
}

function deleteItem(e) {
    e.preventDefault();
    var url = $(this).attr('href');
    var hiddenElement = $(this).parents('tr');
    helper.afterAction = function (response) {
        if (response.success) {
            hiddenElement.hide("slow");
        }
    };
    helper.typeForm = 'post';
    helper.isPushUrl = false;
    helper.isScroll = false;
    helper.sendServer(url, null, true, null);
}
