var helper = {
    typeForm: 'post',
    isPushUrl: false,
    isScroll: true,
    isShowLoader: true,

    afterAction: function () {

    },
    init: function (typeForm, isPushUrl, isScroll, isShowLoader) {
        this.typeForm = typeForm;
        this.isPushUrl = isPushUrl;
        this.isScroll = isScroll;
        this.isShowLoader = isShowLoader !== undefined ? isShowLoader : this.isShowLoader;
        this.afterAction = function (response) {

        };

        return this;
    },
    bootstrapTabs: function (url, container) {
        window.stop();
        this.sendServer(url, null, false, container)
    },
    sendFormById: function (id, isMessage, container) {
        var formData = this.getMapFormData(id, true),
            url = $('#' + id).attr('action');
        this.sendServer(url, formData, isMessage, container);
    },
    sendFilterForm: function (id, isMessage, container) {
        var formData = this.getMapFormData(id, true),
            urlForm = $('#' + id).attr('action'),
            dataUrl = this.getParametersUrl(formData),
            url = urlForm + dataUrl;
        this.sendServer(url, formData, isMessage, container);
    },
    getMapFormData: function (id, isSetOnly) {
        var map = {},
            nodes = document.getElementById(id).elements,
            i1 = 0,
            currentNode;
        while (currentNode = nodes[i1++]) {
            switch (currentNode.nodeName.toLowerCase()) {
                case 'input':
                    if ('checkbox' === currentNode.type.toLowerCase()) {
                        if (isSetOnly && !currentNode.checked) {
                            break;
                        }
                        map[currentNode.name] = currentNode.checked;
                        break;
                    }
                    if (currentNode.value.length) {
                        map[currentNode.name] = currentNode.value;
                    }
                    break;
                case 'textarea':
                    if (currentNode.value.length) {
                        map[currentNode.name] = currentNode.value;
                    }
                    break;
                case 'file':
                    if (currentNode.value.length) {
                        map[currentNode.name] = currentNode.value;
                    }
                    break;
                case 'select':
                    if (currentNode.multiple && currentNode.value.length) {
                        map[currentNode.name] = $('#' + currentNode.id).val();
                    } else if (currentNode.value.length) {
                        map[currentNode.name] = currentNode.value;
                    } else if (!isSetOnly) {
                        map[currentNode.name] = currentNode.value;
                    }
                    break;
            }
        }

        return map;
    },
    sendServer: function (url, data, isMessage, container, isSetAdditionalParams = false) {
        if (this.isShowLoader) {
            $('.loader-content').removeClass('hidden');
        }
        if (!data) {
            data = {};
        }
        if (this.isPushUrl) {
            history.pushState(null, null, url);
        }
        var typeForm = this.typeForm,
            isScroll = this.isScroll,
            ajaxParams = {
                type: typeForm,
                url: url,
                data: data,
                success: function (response) {
                    helper.disableFieldsForAjax(false);
                    if (typeof response != 'object') {
                        response = JSON.parse(response);
                    }
                    if (container && response.html) {
                        $(container).html(response.html);
                    }
                    if (response.success) {
                        if (isMessage || response.showMessage) {
                            showToast('success', response.messages);
                        }
                    } else {
                        if (isMessage || response.showMessage) {
                            showToast('error', response.messages);
                        }
                    }
                    if (isScroll) {
                        window.scroll(0, 0);
                    }
                    helper.afterAction(response);
                    $('.loader-content').addClass('hidden');
                },
                error: function (jqXHR, exception) {
                    console.log('jqXHR', jqXHR);
                    console.log('exception', exception);
                    $('.loader-content').addClass('hidden');
                },
                complete: function () {
                    $('.loader-content').addClass('hidden');
                }
            },
            additionalParams = {
                processData: false,
                contentType: false,
            };
        if (isSetAdditionalParams === true) {
            ajaxParams = $.extend(ajaxParams, additionalParams);
        }
        this.disableFieldsForAjax(true);
        $.ajax(ajaxParams);
    },
    getParametersUrl: function (dataForm) {
        var isBegin = true,
            url = '',
            nameAttribute;
        for (nameAttribute in dataForm) {
            if (isBegin) {
                url = '?' + nameAttribute + '=' + dataForm[nameAttribute]
            } else {
                url = url + '&' + nameAttribute + '=' + dataForm[nameAttribute]
            }
            isBegin = false;
        }

        return url;
    },
    disableFieldsForAjax: function (isDisable) {
        if (isDisable) {
            $('.action-ajax').addClass('disabled');
            $('.action-ajax-select').siblings('span').addClass('select2-container--disabled');
        } else {
            $('.action-ajax').removeClass('disabled');
            $('.action-ajax-select').siblings('span').removeClass('select2-container--disabled');
        }
    }
};

function AjaxHelper(typeForm, isPushUrl, isScroll, isShowLoader) {
    let ajaxObject = {
        typeForm: typeForm === undefined ? 'post' : typeForm,
        isPushUrl: isPushUrl === undefined ? false : isPushUrl,
        isScroll: isScroll === undefined ? true : isScroll,
        isShowLoader: isShowLoader === undefined ? true : isShowLoader,
    };
    this.sendFormById = function (id, isMessage, container) {
        let formData = this.getMapFormData(id, true),
            url = $('#' + id).attr('action');
        this.sendServer(url, formData, isMessage, container);
    };
    this.getMapFormData = function (id, isSetOnly) {
        let map = {},
            nodes = document.getElementById(id).elements,
            i1 = 0,
            currentNode;
        while (currentNode = nodes[i1++]) {
            switch (currentNode.nodeName.toLowerCase()) {
                case 'input':
                    if ('checkbox' === currentNode.type.toLowerCase()) {
                        if (isSetOnly && !currentNode.checked) {
                            break;
                        }
                        map[currentNode.name] = currentNode.checked;
                        break;
                    }
                    if (currentNode.value.length) {
                        map[currentNode.name] = currentNode.value;
                    }
                    break;
                case 'textarea':
                    if (currentNode.value.length) {
                        map[currentNode.name] = currentNode.value;
                    }
                    break;
                case 'file':
                    if (currentNode.value.length) {
                        map[currentNode.name] = currentNode.value;
                    }
                    break;
                case 'select':
                    if (currentNode.multiple && currentNode.value.length) {
                        map[currentNode.name] = $('#' + currentNode.id).val();
                    } else if (currentNode.value.length) {
                        map[currentNode.name] = currentNode.value;
                    } else if (!isSetOnly) {
                        map[currentNode.name] = currentNode.value;
                    }
                    break;
            }
        }

        return map;
    };
    this.sendServer = function (url, data = null, isMessage = false, container = null, isSetAdditionalParams = false) {
        if (ajaxObject.isShowLoader) {
            $('.loader-content').removeClass('hidden');
        }
        if (!data) {
            data = {};
        }
        if (ajaxObject.isPushUrl) {
            history.pushState(null, null, url);
        }
        let typeForm = ajaxObject.typeForm,
            isScroll = ajaxObject.isScroll,
            ajaxParams = {
                type: typeForm,
                url: url,
                data: data,
                success: function (response) {
                    disableFieldsForAjax(false);
                    // console.log('response', response);
                    if (typeof response != 'object') {
                        response = JSON.parse(response);
                    }
                    if (container && response.html) {
                        $(container).html(response.html);
                    }
                    if (response.success) {
                        if (isMessage || response.showMessage) {
                            showToast('success', response.messages);
                        }
                        afterAction(response);
                    } else {
                        if (isMessage || response.showMessage) {
                            showToast('error', response.messages);
                        }
                    }
                    if (isScroll) {
                        window.scroll(0, 0);
                    }
                    $('.loader-content').addClass('hidden');
                },
                error: function (jqXHR, exception) {
                    console.log('jqXHR', jqXHR);
                    console.log('exception', exception);
                    $('.loader-content').addClass('hidden');
                    showToast('error', 'Что то пошло не так... Напишите на vv_support со скриншотом ошибки');
                },
                complete: function () {
                    $('.loader-content').addClass('hidden');
                }
            },
            additionalParams = {
                processData: false,
                contentType: false,
            };
        if (isSetAdditionalParams === true) {
            ajaxParams = $.extend(ajaxParams, additionalParams);
        }
        disableFieldsForAjax(true);
        $.ajax(ajaxParams);
    };
    let disableFieldsForAjax = function (isDisable) {
        if (isDisable) {
            $('.action-ajax').addClass('disabled');
            $('.action-ajax-select').siblings('span').addClass('select2-container--disabled');
        } else {
            $('.action-ajax').removeClass('disabled');
            $('.action-ajax-select').siblings('span').removeClass('select2-container--disabled');
        }
    };
    let afterAction = function () {

    };
    this.setAfterAction = function (afterActionFunction) {
        afterAction = afterActionFunction;
    };
    this.sendFormById = function (id, isMessage, container) {
        let formData = this.getMapFormData(id, true),
            url = $('#' + id).attr('action');
        this.sendServer(url, formData, isMessage, container);
    };
    this.getMapFormData = function (id, isSetOnly) {
        let map = {},
            nodes = document.getElementById(id).elements,
            i = 0,
            currentNode;
        while (currentNode = nodes[i++]) {
            switch (currentNode.nodeName.toLowerCase()) {
                case 'input':
                    if ('checkbox' === currentNode.type.toLowerCase()) {
                        if (isSetOnly && !currentNode.checked) {
                            break;
                        }
                        map[currentNode.name] = currentNode.checked;
                        break;
                    }
                    if (currentNode.value.length) {
                        map[currentNode.name] = currentNode.value;
                    }
                    break;
                case 'textarea':
                    if (currentNode.value.length) {
                        map[currentNode.name] = currentNode.value;
                    }
                    break;
                case 'file':
                    if (currentNode.value.length) {
                        map[currentNode.name] = currentNode.value;
                    }
                    break;
                case 'select':
                    if (currentNode.multiple && currentNode.value.length) {
                        map[currentNode.name] = $('#' + currentNode.id).val();
                    } else if (currentNode.value.length) {
                        map[currentNode.name] = currentNode.value;
                    } else if (!isSetOnly) {
                        map[currentNode.name] = currentNode.value;
                    }
                    break;
            }
        }

        return map;
    };
}
