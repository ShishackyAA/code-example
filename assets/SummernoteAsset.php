<?php

namespace frontend\assets;

use Yii;
use yii\web\AssetBundle;
use yii\helpers\Json;

class SummernoteAsset extends AssetBundle
{
    public $sourcePath = '@bower/summernote/';

    public $depends = [
        'frontend\assets\AppAsset',
    ];

    public function init()
    {
        $this->css[] = 'dist/summernote.css';
        $this->js[] = 'dist/summernote' . (YII_DEBUG ? '' : '.min') . '.js';
        $this->js[] = 'lang/summernote-' . Yii::$app->language . '.js';
        //$this->js[] = 'plugin/summernote-ext-video.js';

        if (!isset($this->publishOptions['only'])) $this->publishOptions['only'] = [];
        $this->publishOptions['only'][] = 'dist/*';
        $this->publishOptions['only'][] = 'lang/*';
        //$this->publishOptions['only'][] = 'plugin/summernote-ext-video.js';

        parent::init();
    }

    public function registerAssetFiles($view)
    {
        parent::registerAssetFiles($view);

        $selector = '.summernote';
        $clientOptions = [
            'lang' => Yii::$app->language,
            'height' => 130,
            'toolbar' => [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview']],
                ['help', ['help']],
            ],
        ];

        $view->registerJs('jQuery("' . $selector . '").summernote(' . Json::encode($clientOptions) . ');');
    }
}
