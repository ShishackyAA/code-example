<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class AppAsset
 * @package frontend\assets
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@frontend/assets/app';
    public $css = [
        'css/font-awesome.css',
        'css/animate.css',
        'css/style.css',
        'css/c-menu.css',
        'css/custom.css',
        'css/loader.css',
        // Удалить при рефакторинге и файл тоже
        'css/output.css',
        'css/contact.css',
        'css/switchery.min.css',

    ];
    public $js = [
        'js/plugins/metisMenu/jquery.metisMenu.js',
        'js/plugins/slimscroll/jquery.slimscroll.min.js',
        'js/inspinia.js',
        'js/plugins/sparkline/jquery.sparkline.min.js',
        'js/slide-menu.js',
        'js/main.js',
        'js/auto-modal.js',
        //хз че это, старый Венго
        'js/layout.js',
        'js/jquery.mCustomScrollbar.js',
        'js/switchery.min.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'odaialali\yii2toastr\ToastrAsset'
    ];
}
