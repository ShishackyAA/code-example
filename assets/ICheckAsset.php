<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Description of ICheckAsset
 *
 * @author Vostrikov Michael
 */
class ICheckAsset extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/Icheck';
    public $css = [
        'skins/square/_all.css',
        'skins/square/green.css',
    ];
    
    public $js = [
        'icheck.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    
    public function registerAssetFiles($view)
    {
        parent::registerAssetFiles($view);
        
        $script = "
            $(document).ready(function(){
                $('input.icheck').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green'
                });
            });
        ";
        $view->registerJs($script, $view::POS_END);
    }
}
