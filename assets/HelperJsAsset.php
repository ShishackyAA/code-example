<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Description of LoginAsset
 *
 * @author Rudakov Alexey
 */
class HelperJsAsset extends AssetBundle
{
    public $sourcePath = '@frontend/assets/helper';
    public $css = [

    ];

    public $js = [
        'js/index.js',
        'js/modulesFunctions.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
