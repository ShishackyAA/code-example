var time = 60000 * 3;
$(function () {
    $('[data-toggle="popover"]').popover()
});
$(document).ready(function() {

    window.onscroll = function() {
        $('.popover').each(function () {
            $(this).remove();
        });
    };

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    /* MINIMIZE LEFT MENU */
    $('body').on('click', '.navbar-minimalize', function(e) {
        var status = $(this).data('status');
        var $this = $('.navbar-minimalize');

        ToggleAppApplet($this, status, ToggleAppApplet_MenuCallback);

        return false;
    });

   // Disabled lock function for development time by request of Denis. A Rudakov.
   // setInterval(checkUserDoing, time);


    $('.right-widget-header').on('click', function() {
        var $content = $(this).next();
        var $this = $(this);
        var status = 'close';

        if ($content.is(':visible')) {
            status = 'open';
        }

        ToggleAppApplet($this, status, ToggleAppApplet_SideCallback);
    });

    function setMinHeightForTabs() {
        $('.wrap-new-contact-tabs .tab-content').each(function() {
            var heights = [];
            $('.tab-pane', this).each(function() {
                heights.push($(this).height());
            });
            $('.tab-pane', this).css("min-height", Math.max.apply(null, heights));
        });
    }
    setMinHeightForTabs();
    var equalSize = function() {
        var mainHeight = $('#main-content').height();
        $('#right-height').height(mainHeight - $('#right-footer').outerHeight());
    }
    setTimeout(equalSize, 800);


    $('#main-content').resize(function() {
        equalSize();
    });

    $(window).resize(function() {
        equalSize();
    });

    /* Select one row in table */
    $('body').on('click', '.selectable-row', function(e) {
        $('.selectable-row').removeClass('checked-color-row');
        $(this).addClass('checked-color-row');
    });

    /* Select multiple row in table */
    $('body').on('click', '.selectable-multi-row', function(e) {
        $(this).toggleClass('checked-color-row');
    });

    /* Modal confirmation */
    $('body').on('click', '.modal-confirm-open', function(e) {
        $(this).next().modal('show');
    });
    $('body').on('click', '.modal-confirm-close', function(e) {
        $(this).parents('.modal').modal('hide');
    });
    $('body').on('click', '.modal-close', function(e) {
        $(this).parents('.modal').modal('hide');
    });

    /* Mark message-notification in dropdown read by hover */
    var delay = 2000, setTimeoutConst;
    $(document).on({
        mouseenter: function() {
            var box = $(this);
            setTimeoutConst = setTimeout(function() {
                MakeNotificationRead(box);
            }, delay);
        },
        mouseleave: function() {
            clearTimeout(setTimeoutConst);
        }
    }, '.dropdown-messages-box');


    /* Mark message-notification in dropdown read by click "Mark as read" */
    $('body').on('click', '.mark-read', function(e) {
        e.stopPropagation();
        var box = $(this).parents('.dropdown-messages-box');
        MakeNotificationRead(box);
        return false;
    });

    //add scroll to each element with .scrolling
    $('.scrolling, #my-apps-widget .right-widget-content > div').mCustomScrollbar({
        theme: 'dark',
        scrollInertia: 0,
        scrollbarPosition: 'inside',
        autoHideScrollbar: true,
        mouseWheel: {
            normalizeDelta: true,
            scrollAmount: 9
        }
    });

    //scroll to element
    $(".click-scroll").click(function() {
        $('html, body').animate({
            scrollTop: $(".to-scroll").offset().top - 70
        }, 1000);

        return false;
    });


    //add ajax handler to bootstrap tab with this class
    $('.ajax-mode-tabs a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        var tab = $(e.target);
        var id = tab.attr('href')
                .substring(1);

        var param = id.split('-');
        param = param[0] ? param[0] : param;

        console.log(id, param);

        var box = $('#' + id).find('.tab-inner');
        var alreadyDone = box.data('load');

        if (!alreadyDone || alreadyDone != 1) {
            $.ajax({
                type: 'GET',
                cache: true,
                dataType: 'html',
                data: {
                    param: param
                },
                success: function(response) {
                    box.html(response);
                    box.data('load', 1);
                }
            });
        }
    });

    //go to url by click the tab with this class
    $('a[data-toggle="tab"].external-link').on('shown.bs.tab', function(e) {
        $(e.relatedTarget).tab('show');
        var tab = $(e.target);
        var url = tab.data('url');

        //go to url
        window.location = url;
    });

    /* Mark message in dialog read by hover */
    $(document).on({
        mouseenter: function() {
            var box = $(this);
            setTimeoutConst = setTimeout(function() {
                MakeMessageRead(box);
            }, delay);
        },
        mouseleave: function() {
            clearTimeout(setTimeoutConst);
        }
    }, '#dialog-message-list .list-messages li');

    //collapse message
    $('#dialog-message-list').on('click', '.collapse-messages[data-ajax=1]', function(e) {
        var box = $(this);
        ExpandMessage(box, 0);
    });

    //collapse all message
    $('#dialog-message-list').on('click', '.collapse-all', function(e) {
        var box = $(this);
        ExpandAllMessage(box, 0);
    });

    //expand message
    $('#dialog-message-list').on('click', 'li.collapsed', function(e) {
        var box = $(this).find('.expand-messages[data-ajax=1]');
        var parentBox = $(this);

        ExpandMessage(box, 1);
        MakeMessageRead(parentBox);
    });

    //expand all message
    $('#dialog-message-list').on('click', '.expand-all', function(e) {
        var box = $(this);
        ExpandAllMessage(box, 1);
    });

    //archive all message into dialod
    $('#dialog-message-list').on('click', '.archive-all', function(e) {
        var box = $(this);

        var id = parseInt(box.data('id'));
        var url = box.data('url');
        var type = box.data('type');
        var text = '';
        var newType = '';

        box.attr('data-ajax', 0);

        if (type == 1) {
            text = box.data('restore-text');
            newType = 0;
        } else {
            text = box.data('archive-text');
            newType = 1;
        }

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: {
                id: id,
                type: type
            },
            success: function(response) {
                box.html(text);
                box.data('ajax', 1);
                box.data('type', newType);
            }
        });
    });

    //archive all message from dialod list
    $('body').on('click', '.dialog-to-archive', function(e) {
        var button = $(this);
        var box = $(this).parents('li');
        var url = button.data('url');
        var id = parseInt(button.data('id'));
        var type = box.parents('.tab-pane').attr('id') == 'archive-messages' ? 0 : 1;

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: {
                id: id,
                type: type
            },
            success: function(response) {
                //delete message from current list
                box.remove();

                //make all list ajax load
                $(".messages-list-wrap .tab-inner").each(function(index) {
                    if (!$(this).parents('.tab-pane').hasClass('active')) {
                        $(this).data('load', 0);
                        $(this).html('<div class="empty-messages">Loading...</div>');
                    }
                });
            }
        });
    });

    // show/hide textarea in user profile
    $('.message-toggle-button').click(function(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        $('form.comment-' + id).toggleClass('hidden');
    });


    //collapse|expand grades: show details list
    $('#grade-list').on('click', '.collapse-button', function(e) {
        var box = $(this);
        var type = $(this).data('type');
        if (type === 'expand' || type === 'collapse') {
            ToggleGrade(box, type);
        } else {
            console.log('Wrong type value');
        }
    });

    //collapse|expand grades: show details list
    $('#grade-list').on('click', '.collapse-all-button', function(e) {
        var box = $(this);
        var type = $(this).data('type');
        if (type === 'expand' || type === 'collapse') {
            ToggleAllGrade(box, type);
        } else {
            console.log('Wrong type value');
        }
    });

    //
    var shortCutSwitchElem = document.querySelector('.short-cut-switch');
    if (shortCutSwitchElem != null)
        var scs = new Switchery(shortCutSwitchElem, { size: 'small' });

    var layoutSwitchElem = document.querySelector('.layout-switch');
    if (layoutSwitchElem != null) {
        var lcs = new Switchery(layoutSwitchElem, {size: 'small'});
    }



    // Configuring hide/show right column switcher
    $('#short-cart-swither-wrapper').on('click',function(e){
        e.stopPropagation();
        return false;
    });

    $('#layout-swither-wrapper').on('click', function (e) {
        e.stopPropagation();
        return false;
    });

    var shortCutSwitch = document.querySelector('.short-cut-switch');
    if (shortCutSwitch) {
        shortCutSwitch.onchange = function () {
            var status = $(this).data('status');
            ToggleAppApplet($(this), status, ToggleAppApplet_RightCallback);
        };
    }





    var elem = document.querySelector('.js-switch');
    if (elem != null)
        var init = new Switchery(elem);

    var changeCheckbox = document.querySelector('.js-switch');

    // out of office - slide filters
    if (changeCheckbox != null) {
        changeCheckbox.onchange = function() {
            if (changeCheckbox.checked) {
                $('.slide-filter').slideDown(300);
                $('.toggle-filter').addClass('active');
            } else {
                $('.slide-filter').slideUp(300);
                $('.toggle-filter').removeClass('active');
            }
        };
    }

    // function for work with modal confirm dialog
    window.confirmModal = function(text, ok_callback, cancel_callback)
    {
        var dialog = $('#confirm-modal');
        dialog.find('.confirm-text').html(text);

        if (ok_callback != undefined)
            dialog.find('.confirm-button-yes').off('click').one('click', ok_callback);
        if (cancel_callback != undefined)
            dialog.find('.confirm-button-no').off('click').one('click', cancel_callback);

        dialog.modal('show');
    }

    $('.submit-confirm').click(function() {
        var element = $(this);
        var confirmText = element.data('confirm-text');

        if (!confirmText) confirmText = 'Are you sure?';
        confirmModal(confirmText, function() {
            element.trigger('click.yii');
        });

        return false;
    });

    $('.submit-confirm-form').click(function() {
        var element = $(this);
        var form = element.parents('form');
        var confirmText = element.data('confirm-text');

        if (!confirmText) confirmText = 'Are you sure?';
        confirmModal(confirmText, function() {
            form.trigger('submit.yii');
        });

        return false;
    });

    ////////////////////////////////////////////////////////////bad code
    $('.submit-confirm-form-profile').click(function() {
        var element = $(this);
        var form = element.parents('form');
        var confirmText = element.data('confirm-text');
        var language = $('#profilesettingsform-language_code').val();
        var timeLock = $('#profilesettingsform-lock_screen_timeout').val();
        if (!language || !timeLock) {
            return false;
        }
        if (!confirmText) confirmText = 'Are you sure?';
        confirmModal(confirmText, function() {
            form.trigger('submit.yii');
        });

        return false;
    });
    ////////////////////////////////////////////////////////////

    // function for work with modal notify dialog
    window.notifyModal = function(title, text, titleHint, iconClass)
    {
        var dialog = $('#notify-modal');
        dialog.find('.confirm-text').html(text);

        dialog.find('.modal-title').html(title);
        dialog.find('.modal-body').html(text);

        dialog.find('.modal-icon').removeClass().addClass('modal-icon');
        if (typeof(iconClass) != 'undefined')
            dialog.find('.modal-icon').addClass('modal-icon fa ' + iconClass);

        dialog.find('.modal-title-hint').html('');
        if (typeof(titleHint) != 'undefined')
            dialog.find('.modal-title-hint').html(titleHint);

        dialog.modal('show');
    }


    $(document).on('pjax:send', function(event) {
        $(event.relatedTarget).addClass('ajax-loading');
    });
    $(document).on('pjax:complete', function(event) {
        $(event.relatedTarget).removeClass('ajax-loading');
    });


    /*
        Emulates Pjax request and allows to change html content via callback before placing into container
        Useful when you have tabs in content and need to change active tab in it
        If blockSelectors array is not empty, only html in these blocks is refreshed
        Selectors should be in single instance on page
    */
    window.pjaxReload = function(pjaxContainerSelector, blockSelectors, contentCallback) {
        $.ajax({
          url: location.href + '&_pjax=' + encodeURIComponent(pjaxContainerSelector),
          beforeSend: function(xhr, settings) {
            xhr.setRequestHeader('X-PJAX', 'true');
            xhr.setRequestHeader('X-PJAX-Container', pjaxContainerSelector);
          },
          success: function(data) {
            var content = $($.parseHTML(data, document, true));

            // remove external scripts because they are already present on page
            scripts = content.filter('script[src]').remove();
            scripts = scripts.add(content.find('script[src]').remove());
            content = content.not(scripts);

            if (typeof(contentCallback) == 'function') contentCallback(content);

            if (blockSelectors instanceof Array && blockSelectors.length > 0) {
                for (var i in blockSelectors) {
                    var selector = blockSelectors[i];
                    var blockHtml = $(selector, content).html();
                    $(selector).html(blockHtml);
                }
            } else {
                $(pjaxContainerSelector).html(content);
            }
          }
        });
    }
});

/* Mark message as read */
var MakeNotificationRead = function(box) {
    var url = box.parents('.dropdown').data('url');
    var id = box.attr('data-id');
    var status = box.attr('data-status');
    if (status == 'new') {
        $.ajax({
            type: 'GET',
            cache: false,
            url: url,
            data: {
                'id': id
            },
            success: function(response) {
                box.attr('data-status', 'old');
                box.find('.media-body').css('background', '#fff');
            }
        });
    }
};

/* Mark message as read */
var MakeMessageRead = function(box) {
    var url = box.attr('data-read-url');
    var id = box.attr('data-id');
    var status = box.attr('data-status');
    if (status === 'new') {
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                'id': id
            },
            success: function(response) {
                box.attr('data-status', 'old');
                box.removeClass('new-message');
            }
        });
    }
};

/* Expand message in dialog */
var ExpandMessage = function(box, expand) {
    var boxParent = box.parents('li');

    var id = boxParent.data('id');
    var url = boxParent.data('url');

    box.attr('data-ajax', 0);

    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'json',
        data: {
            id: id,
            expand: expand
        },
        success: function(response) {
            if (expand == 0) {
                boxParent.addClass('collapsed');
            } else {
                boxParent.removeClass('collapsed');
            }

            box.attr('data-ajax', 1);
        }
    });
};

/* Expand all messages in dialog */
var ExpandAllMessage = function(box, expand) {
    var id = box.data('id');
    var url = box.data('url');

    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'json',
        data: {
            id: id,
            expand: expand
        },
        success: function(response) {
            $(".list-messages li").each(function(index) {
                if (expand == 1) {
                    $(this).removeClass('collapsed');
                } else {
                    $(this).addClass('collapsed');
                }
            });
        }
    });
};


/* Expand|Collapse grade details */
var ToggleGrade = function(box, type) {
    var boxParent = box.parents('li');

    var id = boxParent.data('id');
    var url = boxParent.data('url');

    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'html',
        data: {
            id: id,
            type: type
        },
        success: function(response) {
            box.css('display', 'none');
            box
                    .siblings(".collapse-button")
                    .css('display', 'block');

            boxParent
                    .toggleClass('uncollapsed-row')
                    .find('.wrap-grade-table')
                    .html(response);
        }
    });
};

/* Expand|Collapse all grade details */
var ToggleAllGrade = function(box, type) {
    var boxParent = box.parents('.tab-inner');
    var url = box.parents('.buttons').data('url');

    //get id for all collapsed|expanded grade
    var grade = boxParent.find('.grade-item');
    var ids = [];
    var id = 0;
    grade.each(function(index, value) {
        id = $(this).data('id');
        if (id != 0) {
            ids.push($(this).data('id'));
        }
    });

    if (ids.length > 0) {
        ids = ids.toString();

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: {
                ids: ids,
                type: type
            },
            success: function(response) {
                for (var key in response) {
                    if (response.hasOwnProperty(key)) {
                        grade = $('.grade-item[data-id=' + key + ']');

                        var button = grade.find('.collapse-button[data-type=' + type + ']');

                        button.css('display', 'none');
                        button
                                .siblings(".collapse-button")
                                .css('display', 'block');

                        grade
                                .toggleClass('uncollapsed-row')
                                .find('.wrap-grade-table')
                                .html(response[key]);
                    }
                }
            }
        });
    }
};


var ToggleAppAppletExt = function (id, url, status, callback) {
    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'json',
        data: {
            id: id,
            status: status
        },
        success: function(response) {
            if (typeof(callback) != 'undefined') {
                callback(response);
            }
        }
    });
};

/* Expand|Collapse grade details */
var ToggleAppApplet = function (box, status, callback) {
    var id = box.data('id');
    var url = box.data('url');
    ToggleAppAppletExt(id, url, status, function (response) {
        callback(box);
    });
};

$('.toggle-app-applet').on('change', function() {
    var id = $(this).data('id');
    var url = $(this).data('url');
    var status = $(this).data('status');

    var newStatus;
    if (status == 'open') newStatus = 'close';
    else newStatus = 'open';

    $(this).data('status', newStatus);
    ToggleAppAppletExt(id, url, newStatus);
});


/** Callback for ToggleAppAplet function */
var ToggleAppApplet_SideCallback = function(box) {
    var $content = box.next();
    var html = "=";

    if ($content.is(':visible')) {
        html = '<i class="fa fa-sort"></i>';
    }

    box.find('.pull-right').html(html);

    $content.slideToggle('fast');
    box.find('.header-decorator').toggle();
};

/** Callback for ToggleAppApplet function */
var ToggleAppApplet_MenuCallback = function(box) {
    var $this = $('.navbar-minimalize');
    var status = $this.data('status');
    var sign = '';
    if (status === 'close') {
        $this.data('status', 'open');
        sign = '+';
    } else {
        $this.data('status', 'close');
        sign = '-';
        ToggleLeftMenu();
    }

    $("#main-content").animate({
        marginLeft: sign + '=130px'
    }, 500, function() {
        if (status === 'close') {
            ToggleLeftMenu();
        }
    });
};

/** Callback for ToggleAppApplet function */
var ToggleAppApplet_RightCallback = function(box) {
    var shortCutSwitch = document.querySelector('.short-cut-switch');
    if (!shortCutSwitch) return;

    if(shortCutSwitch.checked){
        $('#main-content').removeClass('margin-right0');
        $('#right-column').removeClass('hide');
    }else{
        $('#main-content').addClass('margin-right0');
        $('#right-column').addClass('hide');
    }
};
/** Callback for ToggleAppApplet function */

$('.layout-switch').change(function() {
    if ($(this).prop('checked')) {
        $('#main-header').removeClass('boxed-header').addClass('full-width-header');
        $('#main-content-wrapper').removeClass('container');
    } else {
        $('#main-header').addClass('boxed-header').removeClass('full-width-header');
        $('#main-content-wrapper').addClass('container');
    }
});

/** Expand|Collapse left menu */
var ToggleLeftMenu = function() {
    $('#logo').toggleClass('short-logo');
    $('#main-nav').find('.user-menu').toggleClass('hidden');
    $('#main-header').toggleClass('header left-open-header');
    $('.footer_main').toggleClass('left_footer');
    $('.blog-filter').toggleClass('blog-filter-mini');

    var curText = $('#logo').text();
    var newText = $('#logo').data('text');

    $('#logo')
            .text(newText)
            .data('text', curText);
};

function getThisURL()
{
    pathArray = location.href.split("://");
    pathArray = pathArray[1].split('/');
    pathArray.shift();
    return pathArray;
}

function checkUserDoing() {
    var url = getThisURL();
    var redircect_url = "";
    for (i = 1; i < url.length; i++)
    {
        redircect_url = url[i - 1] + "/" + url[i];
    }
    $.ajax({
        type: 'POST',
        url: '/site/doing',
        data: {
            redirect: redircect_url
        },
        success: function(response) {
            var obj = $.parseJSON(response)
            if (obj.url !== "")
            {
                window.location.href = obj.url;
            }
        }

    });
}

function pushCurl(url) {
    try {
        history.pushState(null, null, url);
        return;
    } catch(e) {}
    location.hash = '#' + url;
}

function getMapAusForm(id, isSetOnly) {
    var map = {},
        nodes = document.getElementById(id).elements,
        i1 = 0,
        currentNode;
    while (currentNode = nodes[i1++]) {
        switch (currentNode.nodeName.toLowerCase()) {
            case 'input':
                if ('checkbox' === currentNode.type.toLowerCase()) {
                    if (isSetOnly && !currentNode.checked) {
                        break;
                    }
                    map[currentNode.name] = currentNode.checked;
                    break;
                }
                if (currentNode.value.length) {
                    map[currentNode.name] = currentNode.value;
                }
                break;
            case 'textarea':
                if (currentNode.value.length) {
                    map[currentNode.name] = currentNode.value;
                }
                break;
            case 'select':
                if (currentNode.multiple && currentNode.value.length) {
                    map[currentNode.name] = $('#' + currentNode.id).val();
                } else if (currentNode.value.length) {
                    map[currentNode.name] = currentNode.value;
                } else if (!isSetOnly) {
                    map[currentNode.name] = currentNode.value;
                }
                break;
        }
    }

    return map;
}

function getParametersUrl(dataForm) {
    var isBegin = true,
        url = '';
    for(nameAttribute in dataForm){
        if (isBegin) {
            url = '?' + nameAttribute + '=' + dataForm[nameAttribute]
        } else {
            url = url + '&' + nameAttribute + '=' + dataForm[nameAttribute]
        }
        isBegin = false;
    }

    return url;
}
