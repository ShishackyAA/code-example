/**
 * How activate autoModal in bootstrap\Modal
 *
 * Modal::begin([
 *  'toggleButton' => [
 *      'id'=> 'some-id-name'
 *    ]
 * )];
 *
 * In PHP
 * redirect(['url#modal=some-id-name']);
 *
 * It works!
 */

$(document).ready(function () {
    $(document).on('click', '.close', remove);
    show();

});

function show() {
    var url = document.location.toString();
    if (url.match('#modal')) {
        $('#' + url.split('#modal=')[1]).trigger('click');
    }
}

function remove() {
    var url = document.location.toString();
    if (url.match('#')) {
        history.pushState(null, null, url.split('#')[0]);
    }
}